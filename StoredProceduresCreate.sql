SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

ALTER DATABASE AdfsConfiguration SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO

USE AdfsConfiguration

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddServiceStateSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddServiceStateSummary]
	@ServiceObjectType	nvarchar(256),
	@SerialNumber	bigint,
	@SchemaVersionNumber int	
AS
BEGIN
	SET NOCOUNT ON
	INSERT [IdentityServerPolicy].[ServiceStateSummary](ServiceObjectType, SerialNumber, SchemaVersionNumber, LastUpdateTime)
	VALUES (@ServiceObjectType, @SerialNumber, @SchemaVersionNumber, getutcdate())	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of service state summary failed'',10,1)
	END 
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateServiceStateSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateServiceStateSummary]
	@ServiceObjectType	nvarchar(256),
	@SerialNumber	bigint,
	@SchemaVersionNumber int
AS
BEGIN
	SET NOCOUNT ON   	
	UPDATE [IdentityServerPolicy].[ServiceStateSummary]
		SET SerialNumber = @SerialNumber,
		SchemaVersionNumber = @SchemaVersionNumber,
		LastUpdateTime = getutcdate()
	WHERE ServiceObjectType = @ServiceObjectType
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of service state summary failed'',10,1)	
	END
END
'

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAllServiceStateSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAllServiceStateSummary]
AS
BEGIN
	SET NOCOUNT ON   	
	DELETE FROM [IdentityServerPolicy].[ServiceStateSummary]
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Removal of service state summary failed'',10,1)	
	END
END
'

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[IncrementServiceStateSummarySerialNumber]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[IncrementServiceStateSummarySerialNumber]
	@ServiceObjectType	nvarchar(256)
AS
BEGIN
	SET NOCOUNT ON
	IF EXISTS(SELECT * from [IdentityServerPolicy].[SyncProperties] WHERE PropertyName=''Role'' and PropertyValue=''PrimaryComputer'')
	BEGIN
		UPDATE [IdentityServerPolicy].[ServiceStateSummary]
			SET SerialNumber = SerialNumber + 1,
			LastUpdateTime = getutcdate()
		WHERE ServiceObjectType = @ServiceObjectType
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Update of service state summary failed'',10,1)	
		END	
	END
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveServiceStateSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveServiceStateSummary]
	@ServiceObjectType	nvarchar(256)
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM [IdentityServerPolicy].[ServiceStateSummary] WHERE ServiceObjectType = @ServiceObjectType
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Deletion of service state summary failed '',10,1) 
	END 
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetServiceStateSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetServiceStateSummary]
AS
BEGIN
	SET NOCOUNT ON
	SELECT ServiceObjectType, SerialNumber, SchemaVersionNumber, LastUpdateTime FROM [IdentityServerPolicy].[ServiceStateSummary]	
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddServiceObjectTypeRelationships]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddServiceObjectTypeRelationships]
	@ReferenceTo	nvarchar(256),
	@ReferenceFrom	nvarchar(256)
AS
BEGIN
	SET NOCOUNT ON
	INSERT [IdentityServerPolicy].[ServiceObjectTypeRelationships](ReferenceTo, ReferenceFrom)
	VALUES (@ReferenceTo, @ReferenceFrom)	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of of service object type relationship failed'',10,1)
	END 
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateServiceObjectTypeRelationships]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateServiceObjectTypeRelationships]
	@ReferenceTo	nvarchar(256),
	@ReferenceFrom	nvarchar(256)
AS
BEGIN
	SET NOCOUNT ON
	UPDATE [IdentityServerPolicy].[ServiceObjectTypeRelationships]
	SET ReferenceFrom = @ReferenceFrom WHERE ReferenceTo = @ReferenceTo
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Updation of service object type relationship failed '',10,1)
	END 
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveServiceObjectTypeRelationships]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveServiceObjectTypeRelationships]
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM [IdentityServerPolicy].[ServiceObjectTypeRelationships]
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Deletion of of service object type relationship failed'',10,1)
	END 
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetServiceObjectTypeReferences]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetServiceObjectTypeReferences]
	@ReferenceTo	nvarchar(256)
AS
BEGIN
SELECT ReferenceFrom FROM IdentityServerPolicy.ServiceObjectTypeRelationships WHERE ReferenceTo = @ReferenceTo
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddSyncProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddSyncProperties]
	@PropertyName	nvarchar(256),
	@PropertyValue	sql_variant
AS
BEGIN
	SET NOCOUNT ON
	INSERT [IdentityServerPolicy].[SyncProperties](PropertyName, PropertyValue)
	VALUES (@PropertyName, @PropertyValue)	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of service state summary failed'',10,1)
	END 
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateSyncProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateSyncProperties]
	@PropertyName	nvarchar(256),
	@PropertyValue	sql_variant
AS
BEGIN
	SET NOCOUNT ON
	UPDATE [IdentityServerPolicy].[SyncProperties]
	SET PropertyValue = @PropertyValue WHERE PropertyName = @PropertyName	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Updation of sync properties failed for Property '',10,1)
	END 
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveSyncProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveSyncProperty]
	@PropertyName	nvarchar(256)
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM [IdentityServerPolicy].[SyncProperties]
	WHERE PropertyName = @PropertyName	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Deletion of sync property failed for Property '',10,1)
	END 
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveSyncProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveSyncProperties]
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM [IdentityServerPolicy].[SyncProperties]
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Deletion of sync property failed '',10,1)
	END 
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetSyncProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetSyncProperties]
AS
BEGIN
	SET NOCOUNT ON
	SELECT PropertyName, PropertyValue FROM [IdentityServerPolicy].[SyncProperties]
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetSyncProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetSyncProperty]
	@PropertyName nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON
	SELECT PropertyName, PropertyValue FROM [IdentityServerPolicy].[SyncProperties] Where PropertyName = @PropertyName
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[CreateAuthority]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[CreateAuthority]
	@ObjectId uniqueidentifier OUTPUT,
	@Name nvarchar(400),
	@IdentityType nvarchar(400),
	@ProtocolProfiles nvarchar(500) = NULL,
	@SamlAuthenticationRequestParameters int = NULL,
	@SamlAuthenticationRequestIndex int = NULL,
	@SamlAuthenticationRequestProtocolBinding nvarchar(500) = NULL,
	@WSFederationPassiveEndpoint nvarchar(400) = NULL,
	@Enabled bit = 1,
	@Description nvarchar(max) = NULL,
	@AutoUpdateFromPublishedPolicyOption bit = NULL,
	@ConflictWithPublishedPolicy bit = NULL,
	@LastPublishedPolicyCheckTime nvarchar(50) = NULL,
	@LastPublishedPolicyCheckSuccessful int = NULL,
	@LastUpdateFromPublishedPolicyTime nvarchar(50) = NULL,
	@PollPublishedPolicyOption bit = NULL,
	@PublishedPolicyUrl nvarchar(400) = NULL, 
	@EntityId nvarchar(400),
	@Organization nvarchar(max) = NULL,
	@AllowCreate bit = 1,
	@RequiredNameIdFormat nvarchar(400) = NULL,
	@SigningCertificateRevocationVerificationSetting int = 0,
	@WantAuthnRequestsSigned bit = 0,
	@EncryptionCertificate nvarchar(max) = NULL,
	@MustEncryptNameId bit = 0,
	@SignatureAlgorithm nvarchar(400),
	@RemoteObjectId uniqueidentifier = NULL,
	@EncryptionCertificateRevocationVerificationSetting int = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @objectType nvarchar(400)
	DECLARE @outputAuthorityTbl TABLE (ObjectId uniqueidentifier NOT NULL)	
	BEGIN TRAN
	
	INSERT [IdentityServerPolicy].[Authorities](Name, IdentityType, 
					   ProtocolProfiles, SamlAuthenticationRequestParameters, SamlAuthenticationRequestIndex, SamlAuthenticationRequestProtocolBinding, 
					   WSFederationPassiveEndpoint,
					   Enabled, Description,
					   AutoUpdateFromPublishedPolicyOption, ConflictWithPublishedPolicy,
					   LastUpdateFromPublishedPolicyTime, EntityId, Organization, 
					   AllowCreate, RequiredNameIdFormat, SigningCertificateRevocationVerificationSetting,
					   WantAuthnRequestsSigned, EncryptionCertificate, MustEncryptNameId, SignatureAlgorithm,
					   EncryptionCertificateRevocationVerificationSetting)
	OUTPUT INSERTED.AuthorityId INTO @outputAuthorityTbl
	VALUES (@Name, @IdentityType,
			@ProtocolProfiles, @SamlAuthenticationRequestParameters, @SamlAuthenticationRequestIndex, @SamlAuthenticationRequestProtocolBinding,
			@WSFederationPassiveEndpoint,
			@Enabled, @Description,
			@AutoUpdateFromPublishedPolicyOption, @ConflictWithPublishedPolicy,
			@LastUpdateFromPublishedPolicyTime, @EntityId, @Organization,
			@AllowCreate, @RequiredNameIdFormat, @SigningCertificateRevocationVerificationSetting,
			@WantAuthnRequestsSigned, @EncryptionCertificate, @MustEncryptNameId, @SignatureAlgorithm,
			@EncryptionCertificateRevocationVerificationSetting
			)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of authority entry failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
			
	SELECT @ObjectId = ObjectId FROM @outputAuthorityTbl		
	   
	IF @RemoteObjectId IS NOT NULL
	BEGIN
		UPDATE [IdentityServerPolicy].[Authorities] SET AuthorityId = @RemoteObjectId WHERE AuthorityId = @ObjectId
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Creation of authority entry failed'',10,1)
			ROLLBACK TRAN
			GOTO end_of_batch
		END
		SET @ObjectId = @RemoteObjectId		
	END
	
	SET @objectType = ''IssuanceAuthority''
		
	INSERT [IdentityServerPolicy].[MetadataSources](ObjectId, PrincipalObjectType, PollPublishedPolicyOption, 
						PublishedPolicyContents, PublishedPolicyUrl, LastPublishedPolicyCheckSuccessful, 
						LastPublishedPolicyCheckTime)
	VALUES (@ObjectId, @objectType, @PollPublishedPolicyOption, 
				NULL, @PublishedPolicyUrl, @LastPublishedPolicyCheckSuccessful, 
				@LastPublishedPolicyCheckTime)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of metadata source entry failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	COMMIT TRAN
	
	-- if an error is encountered, skip all the way to this point
	end_of_batch:
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthorities]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthorities]
   @AuthorityId uniqueidentifier = NULL,
   @IdentityKey nvarchar(400) = NULL,
   @PollPublishedPolicyOption bit = NULL,
   @EntityId nvarchar(400) = NULL,
   @Name nvarchar(400) = NULL,
   @debug bit = 0
AS
BEGIN
   SET NOCOUNT ON
   DECLARE @sql nvarchar(max),
	   @paramlist nvarchar(max)
   
   SELECT @sql = 
   ''SELECT a.AuthorityId, a.Name, a.IdentityType, 
   	  a.ProtocolProfiles, a.SamlAuthenticationRequestParameters, a.SamlAuthenticationRequestIndex, a.SamlAuthenticationRequestProtocolBinding, 
   	  a.WSFederationPassiveEndpoint,
	  a.Enabled, a.Description, 
	  a.AutoUpdateFromPublishedPolicyOption, a.ConflictWithPublishedPolicy,
	  m.LastPublishedPolicyCheckTime, m.LastPublishedPolicyCheckSuccessful, 
	  a.LastUpdateFromPublishedPolicyTime, m.PollPublishedPolicyOption,
	  m.PublishedPolicyUrl, a.EntityId, a.Organization,
	  a.AllowCreate, a.RequiredNameIdFormat, a.SigningCertificateRevocationVerificationSetting,
	  a.WantAuthnRequestsSigned, a.EncryptionCertificate, a.MustEncryptNameId, a.SignatureAlgorithm,
	  a.EncryptionCertificateRevocationVerificationSetting
	FROM [IdentityServerPolicy].[Authorities] a
	LEFT JOIN [IdentityServerPolicy].[MetadataSources] m ON a.AuthorityId = m.ObjectId 
	WHERE  1 = 1''
   
   IF @AuthorityId IS NOT NULL
		SELECT @sql = @sql + '' AND a.AuthorityId = @AuthorityId''
   
   IF @IdentityKey IS NOT NULL
 		SELECT @sql = @sql + '' AND a.AuthorityId IN (SELECT i.AuthorityId FROM [IdentityServerPolicy].[AuthorityIdentities] i WHERE i.IdentityKey = @IdentityKey)''
 		
   IF @PollPublishedPolicyOption IS NOT NULL
 		SELECT @sql = @sql + '' AND a.PollPublishedPolicyOption = @PollPublishedPolicyOption''
   
   IF @EntityId IS NOT NULL
		SELECT @sql = @sql + '' AND a.EntityId = @EntityId''
		
   IF @Name IS NOT NULL
		SELECT @sql = @sql + '' AND a.Name = @Name''
   
   SELECT @paramlist = ''@AuthorityId	uniqueidentifier, 
						 @IdentityKey 	nvarchar(400),
						 @PollPublishedPolicyOption bit,
						 @EntityId nvarchar(400),
						 @Name nvarchar(400)''

   IF @debug = 1
		PRINT @sql

   EXEC dbo.sp_executesql @sql, @paramlist, @AuthorityId, @IdentityKey, @PollPublishedPolicyOption, @EntityId, @Name
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateAuthority]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateAuthority]
	@ObjectId uniqueidentifier,	
	@Name nvarchar(400) = NULL,
	@IdentityType nvarchar(400) = NULL,
	@ProtocolProfiles nvarchar(500) = NULL,
	@SamlAuthenticationRequestParameters int = NULL,
	@SamlAuthenticationRequestIndex int = NULL,
	@SamlAuthenticationRequestProtocolBinding nvarchar(500) = NULL, 
	@WSFederationPassiveEndpoint nvarchar(400) = NULL,
	@Enabled bit = NULL,
	@Description nvarchar(max) = NULL,
	@AutoUpdateFromPublishedPolicyOption bit = NULL,
	@ConflictWithPublishedPolicy bit = NULL,
	@LastPublishedPolicyCheckTime nvarchar(50) = NULL,
	@LastPublishedPolicyCheckSuccessful int = NULL,
	@LastUpdateFromPublishedPolicyTime nvarchar(50) = NULL,
	@PollPublishedPolicyOption bit = NULL,
	@PublishedPolicyUrl nvarchar(400) = NULL,
	@EntityId nvarchar(400) = NULL,
	@Organization nvarchar(max) = NULL,	
	@AllowCreate bit = NULL,
	@RequiredNameIdFormat nvarchar(400) = NULL,
	@SigningCertificateRevocationVerificationSetting int = NULL,
	@WantAuthnRequestsSigned bit = NULL, 
	@EncryptionCertificate nvarchar(max) = NULL,
	@SignatureAlgorithm nvarchar(400) = NULL,
	@MustEncryptNameId bit = NULL,
	@EncryptionCertificateRevocationVerificationSetting int = NULL,
	@NullProtocolProfiles bit = 0,
	@NullSamlAuthenticationRequestParameters bit = 0,
	@NullSamlAuthenticationRequestIndex bit = 0,
	@NullSamlAuthenticationRequestProtocolBinding bit = 0, 
	@NullWSFederationPassiveEndpoint bit = 0,
	@NullDescription bit = 0,
	@NullAutoUpdateFromPublishedPolicyOption bit = 0,
	@NullConflictWithPublishedPolicy bit = 0,
	@NullLastPublishedPolicyCheckTime bit = 0,
	@NullLastPublishedPolicyCheckSuccessful bit = 0,
	@NullLastUpdateFromPublishedPolicyTime bit = 0,
	@NullPollPublishedPolicyOption bit = 0,
	@NullPublishedPolicyUrl bit = 0,
	@NullOrganization bit = 0,
	@NullRequiredNameIdFormat bit = 0,
	@NullEncryptionCertificate bit = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @objectType nvarchar (400)
	BEGIN TRAN
	
	UPDATE [IdentityServerPolicy].[Authorities]
		SET Name = CASE WHEN @Name IS NULL THEN Name ELSE @Name END,
			IdentityType = CASE WHEN @IdentityType IS NULL THEN IdentityType ELSE @IdentityType END,
			ProtocolProfiles = CASE WHEN @ProtocolProfiles IS NULL THEN ProtocolProfiles ELSE @ProtocolProfiles END,
			SamlAuthenticationRequestParameters = CASE WHEN @SamlAuthenticationRequestParameters IS NULL THEN SamlAuthenticationRequestParameters ELSE @SamlAuthenticationRequestParameters END,
			SamlAuthenticationRequestIndex = CASE WHEN @SamlAuthenticationRequestIndex IS NULL THEN SamlAuthenticationRequestIndex ELSE @SamlAuthenticationRequestIndex END,
			SamlAuthenticationRequestProtocolBinding = CASE WHEN @SamlAuthenticationRequestProtocolBinding IS NULL THEN SamlAuthenticationRequestProtocolBinding ELSE @SamlAuthenticationRequestProtocolBinding END,
			WSFederationPassiveEndpoint = CASE WHEN @WSFederationPassiveEndpoint IS NULL THEN WSFederationPassiveEndpoint ELSE @WSFederationPassiveEndpoint END,
			Enabled = CASE WHEN @Enabled IS NULL THEN Enabled ELSE @Enabled END,
			Description = CASE WHEN @Description IS NULL THEN Description ELSE @Description END,
			AutoUpdateFromPublishedPolicyOption = CASE WHEN @AutoUpdateFromPublishedPolicyOption IS NULL THEN AutoUpdateFromPublishedPolicyOption ELSE @AutoUpdateFromPublishedPolicyOption END,
			ConflictWithPublishedPolicy = CASE WHEN @ConflictWithPublishedPolicy IS NULL THEN ConflictWithPublishedPolicy ELSE @ConflictWithPublishedPolicy END,
			LastUpdateFromPublishedPolicyTime = CASE WHEN @LastUpdateFromPublishedPolicyTime IS NULL THEN LastUpdateFromPublishedPolicyTime ELSE @LastUpdateFromPublishedPolicyTime END,
			EntityId = CASE WHEN @EntityId IS NULL THEN EntityId ELSE @EntityId END,
			Organization = CASE WHEN @Organization IS NULL THEN Organization ELSE @Organization END,
			AllowCreate = CASE WHEN @AllowCreate IS NULL THEN AllowCreate ELSE @AllowCreate END,
			RequiredNameIdFormat = CASE WHEN @RequiredNameIdFormat IS NULL THEN RequiredNameIdFormat ELSE @RequiredNameIdFormat END,
			SigningCertificateRevocationVerificationSetting = CASE WHEN @SigningCertificateRevocationVerificationSetting IS NULL THEN SigningCertificateRevocationVerificationSetting ELSE @SigningCertificateRevocationVerificationSetting END,
			WantAuthnRequestsSigned = CASE WHEN @WantAuthnRequestsSigned IS NULL THEN WantAuthnRequestsSigned ELSE @WantAuthnRequestsSigned END,
			EncryptionCertificate = CASE WHEN @EncryptionCertificate IS NULL THEN EncryptionCertificate ELSE @EncryptionCertificate END,
			MustEncryptNameId = CASE WHEN @MustEncryptNameId IS NULL THEN MustEncryptNameId ELSE @MustEncryptNameId END,
			SignatureAlgorithm = CASE WHEN @SignatureAlgorithm IS NULL THEN SignatureAlgorithm ELSE @SignatureAlgorithm END,
			EncryptionCertificateRevocationVerificationSetting = CASE WHEN @EncryptionCertificateRevocationVerificationSetting IS NULL THEN EncryptionCertificateRevocationVerificationSetting ELSE @EncryptionCertificateRevocationVerificationSetting END
			
	WHERE AuthorityId = @ObjectId
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of authority fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	UPDATE [IdentityServerPolicy].[Authorities]
		SET ProtocolProfiles = CASE WHEN @NullProtocolProfiles = 1 THEN NULL ELSE ProtocolProfiles END,
			SamlAuthenticationRequestParameters = CASE WHEN @NullSamlAuthenticationRequestParameters = 1 THEN NULL ELSE SamlAuthenticationRequestParameters END,
			SamlAuthenticationRequestIndex = CASE WHEN @NullSamlAuthenticationRequestIndex = 1 THEN NULL ELSE SamlAuthenticationRequestIndex END,
			SamlAuthenticationRequestProtocolBinding = CASE WHEN @NullSamlAuthenticationRequestProtocolBinding = 1 THEN NULL ELSE SamlAuthenticationRequestProtocolBinding END,
			WSFederationPassiveEndpoint = CASE WHEN @NullWSFederationPassiveEndpoint = 1 THEN NULL ELSE WSFederationPassiveEndpoint END,
			Description  = CASE WHEN @NullDescription  = 1 THEN NULL ELSE Description  END,
			AutoUpdateFromPublishedPolicyOption = CASE WHEN @NullAutoUpdateFromPublishedPolicyOption = 1 THEN NULL ELSE AutoUpdateFromPublishedPolicyOption END,
			ConflictWithPublishedPolicy = CASE WHEN @NullConflictWithPublishedPolicy = 1 THEN NULL ELSE ConflictWithPublishedPolicy END,
			LastUpdateFromPublishedPolicyTime = CASE WHEN @NullLastUpdateFromPublishedPolicyTime = 1 THEN NULL ELSE LastUpdateFromPublishedPolicyTime END,
			Organization = CASE WHEN @NullOrganization = 1 THEN NULL ELSE Organization END,
			RequiredNameIdFormat = CASE WHEN @NullRequiredNameIdFormat = 1 THEN NULL ELSE RequiredNameIdFormat END,
			EncryptionCertificate = CASE WHEN @NullEncryptionCertificate = 1 THEN NULL ELSE EncryptionCertificate END
	WHERE AuthorityId = @ObjectId
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Reset of authority fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END

	SET @objectType = ''IssuanceAuthority''

	UPDATE [IdentityServerPolicy].[MetadataSources]
		SET LastPublishedPolicyCheckTime = CASE WHEN @LastPublishedPolicyCheckTime IS NULL THEN LastPublishedPolicyCheckTime ELSE @LastPublishedPolicyCheckTime END,
			LastPublishedPolicyCheckSuccessful = CASE WHEN @LastPublishedPolicyCheckSuccessful IS NULL THEN LastPublishedPolicyCheckSuccessful ELSE @LastPublishedPolicyCheckSuccessful END,
			PollPublishedPolicyOption = CASE WHEN @PollPublishedPolicyOption IS NULL THEN PollPublishedPolicyOption ELSE @PollPublishedPolicyOption END,
			PublishedPolicyUrl = CASE WHEN @PublishedPolicyUrl IS NULL THEN PublishedPolicyUrl ELSE @PublishedPolicyUrl END
	WHERE ObjectId = @ObjectId AND PrincipalObjectType = @objectType
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of metadata source fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	UPDATE [IdentityServerPolicy].[MetadataSources]
		SET LastPublishedPolicyCheckTime = CASE WHEN @NullLastPublishedPolicyCheckTime = 1 THEN NULL ELSE LastPublishedPolicyCheckTime END,
			LastPublishedPolicyCheckSuccessful = CASE WHEN @NullLastPublishedPolicyCheckSuccessful = 1 THEN NULL ELSE LastPublishedPolicyCheckSuccessful END,
			PollPublishedPolicyOption = CASE WHEN @NullPollPublishedPolicyOption = 1 THEN NULL ELSE PollPublishedPolicyOption END,
			PublishedPolicyUrl = CASE WHEN @NullPublishedPolicyUrl = 1 THEN NULL ELSE PublishedPolicyUrl END
	WHERE ObjectId = @ObjectId AND PrincipalObjectType = @objectType
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Reset of metadata source fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	COMMIT TRAN
	
	-- if an error is encountered, skip all the way to this point
	end_of_batch:
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[DeleteAuthorities]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[DeleteAuthorities]
	@ObjectId uniqueidentifier = NULL,
	@IdentityKey nvarchar(400) = NULL,
	@debug bit = 0
AS
BEGIN
   SET NOCOUNT ON
   DECLARE @sql nvarchar(max),
	   @paramlist nvarchar(max)
	  
   SELECT @sql = 
   ''DELETE FROM [IdentityServerPolicy].[Authorities] WHERE  1 = 1''
	 
   IF @ObjectId IS NOT NULL
	SELECT @sql = @sql + '' AND AuthorityId = @ObjectId''
	
   IF @IdentityKey IS NOT NULL
 		SELECT @sql = @sql + '' AND AuthorityId IN (SELECT i.AuthorityId FROM [IdentityServerPolicy].[AuthorityIdentities] i WHERE i.IdentityKey = @IdentityKey)''
   
   SELECT @paramlist = ''@ObjectId		uniqueidentifier,
						 @IdentityKey 		nvarchar(400)''

   IF @debug = 1
		PRINT @sql
   
   EXEC dbo.sp_executesql @sql, @paramlist, @ObjectId, @IdentityKey
   
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthorityIdentity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthorityIdentity]
	@ObjectId uniqueidentifier,
	@IdentityKey nvarchar(400),
	@IdentityData nvarchar(max) = NULL
AS
BEGIN
	SET NOCOUNT ON	
	IF EXISTS (SELECT * FROM [IdentityServerPolicy].[Authorities] WHERE AuthorityId = @ObjectId)
		BEGIN
			INSERT [IdentityServerPolicy].[AuthorityIdentities](AuthorityId, IdentityKey, IdentityData)
			VALUES (@ObjectId, @IdentityKey, @IdentityData)			
		END
	ELSE
		RAISERROR(''Could not find authority'',10,1)		
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthorityIdentity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthorityIdentity]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	SELECT a.IdentityData FROM [IdentityServerPolicy].[AuthorityIdentities] a
	WHERE a.AuthorityId = @ObjectId
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthorityIdentity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthorityIdentity]
	@ObjectId uniqueidentifier,
	@IdentityKey nvarchar(400) = NULL,
	@IdentityData nvarchar(max) = NULL,
	@debug bit = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @sql nvarchar(max),
	   @paramlist nvarchar(max)
	
	  
   SELECT @sql = 
   ''DELETE FROM [IdentityServerPolicy].[AuthorityIdentities]
	 WHERE AuthorityId = @ObjectId''
   
   IF @IdentityKey IS NOT NULL
	SELECT @sql = @sql + '' AND IdentityKey = @IdentityKey''
	
   IF @IdentityData IS NOT NULL
	SELECT @sql = @sql + '' AND IdentityData = @IdentityData''
   
   SELECT @paramlist = ''@ObjectId		uniqueidentifier,
						 @IdentityKey 		nvarchar(400),
						 @IdentityData 		nvarchar(max)''

   IF @debug = 1
		PRINT @sql
   
   EXEC dbo.sp_executesql @sql, @paramlist, @ObjectId, @IdentityKey, @IdentityData
   
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthorityContactInfoAddress]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthorityContactInfoAddress]
	@ObjectId uniqueidentifier,
	@ContactInfoAddress nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON		
	INSERT [IdentityServerPolicy].[AuthorityContactInfoAddresses](AuthorityId, ContactInfoAddress)
	VALUES (@ObjectId, @ContactInfoAddress)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthorityContactInfoAddress]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthorityContactInfoAddress]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.ContactInfoAddress FROM [IdentityServerPolicy].[AuthorityContactInfoAddresses] a
	WHERE a.AuthorityId = @ObjectId
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthorityContactInfoAddress]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthorityContactInfoAddress]
	@ObjectId uniqueidentifier,
	@ContactInfoAddress nvarchar(max)  = NULL
AS
BEGIN
	SET NOCOUNT ON
   
	IF @ContactInfoAddress IS NOT NULL
	BEGIN
		DELETE FROM [IdentityServerPolicy].[AuthorityContactInfoAddresses] WHERE AuthorityId = @ObjectId AND ContactInfoAddress = @ContactInfoAddress
	END
	ELSE
	BEGIN
		DELETE FROM [IdentityServerPolicy].[AuthorityContactInfoAddresses] WHERE AuthorityId = @ObjectId
	END
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthorityPolicy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthorityPolicy]
	@ObjectId uniqueidentifier,
	@PolicyData nvarchar(max) = NULL,
	@PolicyType nvarchar(400),
	@PolicyUsage int
AS
BEGIN
	SET NOCOUNT ON
	DECLARE	@policyid uniqueidentifier
	DECLARE @outputPolicyTbl TABLE (ObjectId uniqueidentifier NOT NULL)
		
	BEGIN TRAN
	
	INSERT [IdentityServerPolicy].[Policies](PolicyData, PolicyType, PolicyUsage)
	OUTPUT Inserted.PolicyId INTO @outputPolicyTbl
	VALUES (@PolicyData, @PolicyType, @PolicyUsage)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of policy entry failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END

	SELECT @policyid = ObjectId FROM @outputPolicyTbl
	
	INSERT [IdentityServerPolicy].[AuthorityPolicies](AuthorityId, PolicyId)
	VALUES (@ObjectId, @policyid)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of authority policy mapping failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	COMMIT TRAN
	
	-- if an error is encountered, skip all the way to this point
	end_of_batch:
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthorityPolicies]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthorityPolicies]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT p.PolicyData, p.PolicyType, p.PolicyUsage FROM [IdentityServerPolicy].[Policies] p
	JOIN [IdentityServerPolicy].[AuthorityPolicies] a ON p.PolicyId = a.PolicyId
	WHERE a.AuthorityId = @ObjectId
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthorityPolicy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthorityPolicy]
	@ObjectId uniqueidentifier,
	@PolicyData nvarchar(max) = NULL,
	@PolicyType nvarchar(400),
	@PolicyUsage int
AS
BEGIN
	SET NOCOUNT ON

	-- Delete the AuthorityPolicies entry, the corresponding Policy entry gets deleted by a trigger
	IF @PolicyData IS NOT NULL
		DELETE FROM [IdentityServerPolicy].[AuthorityPolicies]
		FROM [IdentityServerPolicy].[Policies] p 
		JOIN [IdentityServerPolicy].[AuthorityPolicies] a ON a.PolicyId = p.PolicyID 
		WHERE a.AuthorityId = @ObjectId 
			AND p.PolicyType = @PolicyType AND p.PolicyUsage = @PolicyUsage
			AND p.PolicyData = @PolicyData
	ELSE
		DELETE FROM [IdentityServerPolicy].[AuthorityPolicies]
		FROM [IdentityServerPolicy].[Policies] p 
		JOIN [IdentityServerPolicy].[AuthorityPolicies] a ON a.PolicyId = p.PolicyID 
		WHERE a.AuthorityId = @ObjectId 
			AND p.PolicyType = @PolicyType AND p.PolicyUsage = @PolicyUsage
			AND p.PolicyData IS NULL			
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[CreateScope]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[CreateScope]
	@ObjectId uniqueidentifier OUTPUT,
	@Name nvarchar(400),
	@IdentityType nvarchar(400),
	@ProtocolProfiles nvarchar(500) = NULL,
	@TokenLifetime bigint = NULL,
	@WSFederationPassiveEndpoint nvarchar(400) = NULL,
	@Enabled bit = 1,
	@Description nvarchar(max) = NULL,
	@AutoUpdateFromPublishedPolicyOption bit = NULL,
	@ConflictWithPublishedPolicy bit = NULL,
	@LastPublishedPolicyCheckTime nvarchar(50) = NULL,
	@LastPublishedPolicyCheckSuccessful int = NULL,
	@LastUpdateFromPublishedPolicyTime nvarchar(50) = NULL,
	@PollPublishedPolicyOption bit = NULL,
	@PublishedPolicyUrl nvarchar(400) = NULL,
	@EntityId nvarchar(400) = NULL,
	@Organization nvarchar(max) = NULL,
	@EncryptionCertificate nvarchar(max) = NULL,
	@DisableTokenEncryption bit = NULL,
	@SigningCertificateRevocationVerificationSetting int = 0,
	@EncryptionCertificateRevocationVerificationSetting int = 0,
	@AuthnRequestsSigned bit = 0,
	@WantAssertionsSigned bit = 0,
	@MustEncryptNameId bit = 0,
	@SignatureAlgorithm nvarchar(400),
	@SamlResponseSignatureType int = 1,
	@RemoteObjectId uniqueidentifier = NULL,
	@NotBeforeSkew int = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @objectType nvarchar (400)
	DECLARE @outputScopeTbl TABLE (ObjectId uniqueidentifier NOT NULL)
		
	BEGIN TRAN
	
	INSERT [IdentityServerPolicy].[Scopes](Name, IdentityType, 
					   ProtocolProfiles, TokenLifetime, 
					   WSFederationPassiveEndpoint, 
					   Enabled, Description, 
					   AutoUpdateFromPublishedPolicyOption, ConflictWithPublishedPolicy,
					   LastUpdateFromPublishedPolicyTime, EntityId, Organization, 
					   EncryptionCertificate, DisableTokenEncryption,
					   SigningCertificateRevocationVerificationSetting, 
					   EncryptionCertificateRevocationVerificationSetting,
					   AuthnRequestsSigned, WantAssertionsSigned, MustEncryptNameId, SignatureAlgorithm, SamlResponseSignatureType, 
					   NotBeforeSkew)
	OUTPUT INSERTED.ScopeId INTO @outputScopeTbl
	VALUES (@Name, @IdentityType,
			@ProtocolProfiles, @TokenLifetime,
			@WSFederationPassiveEndpoint,  
			@Enabled, @Description,
			@AutoUpdateFromPublishedPolicyOption, @ConflictWithPublishedPolicy,
			@LastUpdateFromPublishedPolicyTime,	@EntityId, @Organization, 
			@EncryptionCertificate, @DisableTokenEncryption,
			@SigningCertificateRevocationVerificationSetting,
			@EncryptionCertificateRevocationVerificationSetting,
			@AuthnRequestsSigned, @WantAssertionsSigned, @MustEncryptNameId, @SignatureAlgorithm, @SamlResponseSignatureType, 
			@NotBeforeSkew)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of scope entry failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	SELECT @ObjectId = ObjectId FROM @outputScopeTbl
	IF @RemoteObjectId IS NOT NULL	
	BEGIN
		UPDATE [IdentityServerPolicy].[Scopes] SET ScopeId = @RemoteObjectId WHERE ScopeId = @ObjectId
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Creation of scope entry failed'',10,1)
			ROLLBACK TRAN
			GOTO end_of_batch
		END
		SET @ObjectId = @RemoteObjectId
	END
	
	SET @objectType = ''IssuanceScope''

	INSERT [IdentityServerPolicy].[MetadataSources](ObjectId, PrincipalObjectType, PollPublishedPolicyOption, 
						PublishedPolicyContents, PublishedPolicyUrl, LastPublishedPolicyCheckSuccessful, 
						LastPublishedPolicyCheckTime)
	VALUES (@ObjectId, @objectType, @PollPublishedPolicyOption, 
				NULL, @PublishedPolicyUrl, @LastPublishedPolicyCheckSuccessful, 
				@LastPublishedPolicyCheckTime)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of metadata source entry failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END	
   
	COMMIT TRAN
	
	-- if an error is encountered, skip all the way to this point
	end_of_batch:
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopes]
	@ScopeId uniqueidentifier = NULL,
	@IdentityKey nvarchar(400) = NULL,
	@MatchIdentityKey nvarchar(402) = NULL,
	@PollPublishedPolicyOption bit = NULL,
	@EntityId nvarchar(400) = NULL,
	@Name nvarchar(400) = NULL,
	@debug bit = 0
AS
BEGIN
   SET NOCOUNT ON
   DECLARE @sql nvarchar(max),
	   @paramlist nvarchar(max)
   
   SELECT @sql = 
   ''SELECT s.ScopeId, s.Name, s.IdentityType, 
   	  s.ProtocolProfiles, s.TokenLifetime, s.WSFederationPassiveEndpoint,
   	  s.Enabled, s.Description,
	  s.AutoUpdateFromPublishedPolicyOption, s.ConflictWithPublishedPolicy,
	  m.LastPublishedPolicyCheckTime, m.LastPublishedPolicyCheckSuccessful, 
	  s.LastUpdateFromPublishedPolicyTime, m.PollPublishedPolicyOption,
	  m.PublishedPolicyUrl, s.EntityId, s.Organization, 
	  s.EncryptionCertificate, s.DisableTokenEncryption,
	  s.SigningCertificateRevocationVerificationSetting,
	  s.EncryptionCertificateRevocationVerificationSetting,
	  s.AuthnRequestsSigned,
	  s.WantAssertionsSigned,
	  s.MustEncryptNameId,
	  s.SignatureAlgorithm,
	  s.SamlResponseSignatureType,
	  s.NotBeforeSkew
	  
	FROM [IdentityServerPolicy].[Scopes] s
	LEFT JOIN [IdentityServerPolicy].[MetadataSources] m ON s.ScopeId = m.ObjectId 
	WHERE  1 = 1''
   
   
   IF @ScopeId IS NOT NULL
	SELECT @sql = @sql + '' AND s.ScopeId = @ScopeId''

   IF @PollPublishedPolicyOption IS NOT NULL
	SELECT @sql = @sql + '' AND e.PollPublishedPolicyOption = @PollPublishedPolicyOption''	
	
   IF @MatchIdentityKey IS NOT NULL
 	SELECT @sql = @sql + '' AND s.ScopeId IN (SELECT i.ScopeId FROM [IdentityServerPolicy].[ScopeIdentities] i WHERE i.IdentityKey LIKE @MatchIdentityKey)''
   ELSE
	IF @IdentityKey IS NOT NULL
		SELECT @sql = @sql + '' AND s.ScopeId IN (SELECT i.ScopeId FROM [IdentityServerPolicy].[ScopeIdentities] i WHERE i.IdentityKey = @IdentityKey)''

   IF @EntityId IS NOT NULL
	SELECT @sql = @sql + '' AND s.EntityId = @EntityId''
	
   IF @Name IS NOT NULL
	SELECT @sql = @sql + '' AND s.Name = @Name''

   SELECT @paramlist = ''@ScopeId	uniqueidentifier,
						 @IdentityKey 		nvarchar(400),
						 @MatchIdentityKey	nvarchar(402),
						 @PollPublishedPolicyOption bit,
						 @EntityId nvarchar(400),
						 @Name nvarchar(400)''

   IF @debug = 1
		PRINT @sql

   EXEC dbo.sp_executesql @sql, @paramlist, @ScopeId, @IdentityKey, @MatchIdentityKey, @PollPublishedPolicyOption, @EntityId, @Name
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateScope]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateScope]
	@ObjectId uniqueidentifier,	
	@Name nvarchar(400) = NULL,
	@IdentityType nvarchar(400) = NULL,
	@ProtocolProfiles nvarchar(500) = NULL,
	@TokenLifetime bigint = NULL,
	@WSFederationPassiveEndpoint nvarchar(400) = NULL,
	@Enabled bit = NULL,
	@Description nvarchar(max) = NULL,
	@AutoUpdateFromPublishedPolicyOption bit = NULL,
	@ConflictWithPublishedPolicy bit = NULL,
	@LastPublishedPolicyCheckTime nvarchar(50) = NULL,
	@LastPublishedPolicyCheckSuccessful int = NULL,
	@LastUpdateFromPublishedPolicyTime nvarchar(50) = NULL,
	@PollPublishedPolicyOption bit = NULL,
	@PublishedPolicyUrl nvarchar(400) = NULL,
	@EntityId nvarchar(400) = NULL,	
	@Organization nvarchar(max) = NULL,
	@EncryptionCertificate nvarchar(max) = NULL,
	@DisableTokenEncryption bit = NULL,
	@SigningCertificateRevocationVerificationSetting int = NULL,
	@EncryptionCertificateRevocationVerificationSetting int = NULL,
	@AuthnRequestsSigned bit = NULL,
	@WantAssertionsSigned bit = NULL,
	@MustEncryptNameId bit = NULL,
	@SignatureAlgorithm nvarchar(400) = NULL,
	@SamlResponseSignatureType int = NULL,
	@NotBeforeSkew int = NULL,
	@NullProtocolProfiles bit = 0,
	@NullTokenLifetime bit = 0,
	@NullWSFederationPassiveEndpoint bit = 0,
	@NullDescription bit = 0,
	@NullAutoUpdateFromPublishedPolicyOption bit = 0,
	@NullConflictWithPublishedPolicy bit = 0,
	@NullLastPublishedPolicyCheckTime bit = 0,
	@NullLastPublishedPolicyCheckSuccessful bit = 0,
	@NullLastUpdateFromPublishedPolicyTime bit = 0,
	@NullPollPublishedPolicyOption bit = 0,
	@NullPublishedPolicyUrl bit = 0,
	@NullEntityId bit = 0,
	@NullOrganization bit = 0,
	@NullEncryptionCertificate bit = 0,
	@NullDisableTokenEncryption bit = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @objectType nvarchar (400)
	BEGIN TRAN

	UPDATE [IdentityServerPolicy].[Scopes]
		SET Name = CASE WHEN @Name IS NULL THEN Name ELSE @Name END,
			IdentityType = CASE WHEN @IdentityType IS NULL THEN IdentityType ELSE @IdentityType END,
			ProtocolProfiles = CASE WHEN @ProtocolProfiles IS NULL THEN ProtocolProfiles ELSE @ProtocolProfiles END,
			TokenLifetime = CASE WHEN @TokenLifetime IS NULL THEN TokenLifetime ELSE @TokenLifetime END,
			WSFederationPassiveEndpoint = CASE WHEN @WSFederationPassiveEndpoint IS NULL THEN WSFederationPassiveEndpoint ELSE @WSFederationPassiveEndpoint END,
			Enabled = CASE WHEN @Enabled IS NULL THEN Enabled ELSE @Enabled END,
			Description = CASE WHEN @Description IS NULL THEN Description ELSE @Description END,
			AutoUpdateFromPublishedPolicyOption = CASE WHEN @AutoUpdateFromPublishedPolicyOption IS NULL THEN AutoUpdateFromPublishedPolicyOption ELSE @AutoUpdateFromPublishedPolicyOption END,
			ConflictWithPublishedPolicy = CASE WHEN @ConflictWithPublishedPolicy IS NULL THEN ConflictWithPublishedPolicy ELSE @ConflictWithPublishedPolicy END,
			LastUpdateFromPublishedPolicyTime = CASE WHEN @LastUpdateFromPublishedPolicyTime IS NULL THEN LastUpdateFromPublishedPolicyTime ELSE @LastUpdateFromPublishedPolicyTime END,
			EntityId = CASE WHEN @EntityId IS NULL THEN EntityId ELSE @EntityId END,
			Organization = CASE WHEN @Organization IS NULL THEN Organization ELSE @Organization END,
			EncryptionCertificate = CASE WHEN @EncryptionCertificate IS NULL THEN EncryptionCertificate ELSE @EncryptionCertificate END,
			DisableTokenEncryption = CASE WHEN @DisableTokenEncryption IS NULL THEN DisableTokenEncryption ELSE @DisableTokenEncryption END,
			SigningCertificateRevocationVerificationSetting = CASE WHEN @SigningCertificateRevocationVerificationSetting IS NULL THEN SigningCertificateRevocationVerificationSetting ELSE @SigningCertificateRevocationVerificationSetting END,
			EncryptionCertificateRevocationVerificationSetting = CASE WHEN @EncryptionCertificateRevocationVerificationSetting IS NULL THEN EncryptionCertificateRevocationVerificationSetting ELSE @EncryptionCertificateRevocationVerificationSetting END,
			AuthnRequestsSigned = CASE WHEN @AuthnRequestsSigned IS NULL THEN AuthnRequestsSigned ELSE @AuthnRequestsSigned END,
			WantAssertionsSigned = CASE WHEN @WantAssertionsSigned IS NULL THEN WantAssertionsSigned ELSE @WantAssertionsSigned END,
			MustEncryptNameId = CASE WHEN @MustEncryptNameId IS NULL THEN MustEncryptNameId ELSE @MustEncryptNameId END,
			SignatureAlgorithm = CASE WHEN @SignatureAlgorithm IS NULL THEN SignatureAlgorithm ELSE @SignatureAlgorithm END,
			SamlResponseSignatureType = CASE WHEN @SamlResponseSignatureType IS NULL THEN SamlResponseSignatureType ELSE @SamlResponseSignatureType END,
			NotBeforeSkew = CASE WHEN @NotBeforeSkew IS NULL THEN NotBeforeSkew ELSE @NotBeforeSkew END
			
	WHERE ScopeId = @ObjectId
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of scope fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	UPDATE [IdentityServerPolicy].[Scopes]
		SET ProtocolProfiles = CASE WHEN @NullProtocolProfiles = 1 THEN NULL ELSE ProtocolProfiles END,
			TokenLifetime = CASE WHEN @NullTokenLifetime = 1 THEN NULL ELSE TokenLifetime END,
			WSFederationPassiveEndpoint = CASE WHEN @NullWSFederationPassiveEndpoint = 1 THEN NULL ELSE WSFederationPassiveEndpoint END,
			Description  = CASE WHEN @NullDescription  = 1 THEN NULL ELSE Description  END,
			AutoUpdateFromPublishedPolicyOption = CASE WHEN @NullAutoUpdateFromPublishedPolicyOption = 1 THEN NULL ELSE AutoUpdateFromPublishedPolicyOption END,
			ConflictWithPublishedPolicy = CASE WHEN @NullConflictWithPublishedPolicy = 1 THEN NULL ELSE ConflictWithPublishedPolicy END,
			LastUpdateFromPublishedPolicyTime = CASE WHEN @NullLastUpdateFromPublishedPolicyTime = 1 THEN NULL ELSE LastUpdateFromPublishedPolicyTime END,
			EntityId = CASE WHEN @NullEntityId = 1 THEN NULL ELSE EntityId END,
			Organization = CASE WHEN @NullOrganization = 1 THEN NULL ELSE Organization END,
			EncryptionCertificate = CASE WHEN @NullEncryptionCertificate = 1 THEN NULL ELSE EncryptionCertificate END,
			DisableTokenEncryption = CASE WHEN @NullDisableTokenEncryption = 1 THEN NULL ELSE DisableTokenEncryption END
			
	WHERE ScopeId = @ObjectId
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Reset of scope fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
 	SET @objectType = ''IssuanceScope''

	UPDATE [IdentityServerPolicy].[MetadataSources]
		SET LastPublishedPolicyCheckTime = CASE WHEN @LastPublishedPolicyCheckTime IS NULL THEN LastPublishedPolicyCheckTime ELSE @LastPublishedPolicyCheckTime END,
			LastPublishedPolicyCheckSuccessful = CASE WHEN @LastPublishedPolicyCheckSuccessful IS NULL THEN LastPublishedPolicyCheckSuccessful ELSE @LastPublishedPolicyCheckSuccessful END,
			PollPublishedPolicyOption = CASE WHEN @PollPublishedPolicyOption IS NULL THEN PollPublishedPolicyOption ELSE @PollPublishedPolicyOption END,
			PublishedPolicyUrl = CASE WHEN @PublishedPolicyUrl IS NULL THEN PublishedPolicyUrl ELSE @PublishedPolicyUrl END
	WHERE ObjectId = @ObjectId AND PrincipalObjectType = @objectType
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of metadata source fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	UPDATE [IdentityServerPolicy].[MetadataSources]
		SET LastPublishedPolicyCheckTime = CASE WHEN @NullLastPublishedPolicyCheckTime = 1 THEN NULL ELSE LastPublishedPolicyCheckTime END,
			LastPublishedPolicyCheckSuccessful = CASE WHEN @NullLastPublishedPolicyCheckSuccessful = 1 THEN NULL ELSE LastPublishedPolicyCheckSuccessful END,
			PollPublishedPolicyOption = CASE WHEN @NullPollPublishedPolicyOption = 1 THEN NULL ELSE PollPublishedPolicyOption END,
			PublishedPolicyUrl = CASE WHEN @NullPublishedPolicyUrl = 1 THEN NULL ELSE PublishedPolicyUrl END
	WHERE ObjectId = @ObjectId AND PrincipalObjectType = @objectType
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Reset of metadata source fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END  
   
	COMMIT TRAN
	
	-- if an error is encountered, skip all the way to this point
	end_of_batch:
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[DeleteScopes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[DeleteScopes]
	@ObjectId uniqueidentifier = NULL,
	@IdentityKey nvarchar(400) = NULL,
	@MatchIdentityKey nvarchar(402) = NULL,
	@debug bit = 0
AS
BEGIN
   SET NOCOUNT ON
   DECLARE @sql nvarchar(max),
		   @paramlist nvarchar(max)
	  
	SELECT @sql = ''DELETE FROM [IdentityServerPolicy].[Scopes] WHERE 1=1''
	
	
	IF @ObjectId IS NOT NULL
		SELECT @sql = @sql + '' AND ScopeId = @ObjectId''
   
	IF @MatchIdentityKey IS NOT NULL
 		SELECT @sql = @sql + '' AND ScopeId IN (SELECT ScopeId FROM [IdentityServerPolicy].[ScopeIdentities] WHERE IdentityKey LIKE @MatchIdentityKey)''
	ELSE
		IF @IdentityKey IS NOT NULL
			SELECT @sql = @sql + '' AND ScopeId IN (SELECT ScopeId FROM [IdentityServerPolicy].[ScopeIdentities] WHERE IdentityKey = @IdentityKey)''
   
	IF @debug = 1
		PRINT @sql
	
	SELECT	@paramlist = ''@ObjectId	uniqueidentifier,
			@IdentityKey 		nvarchar(400),
			@MatchIdentityKey	nvarchar(402)''
			
	EXEC dbo.sp_executesql @sql, @paramlist, @ObjectId, @IdentityKey, @MatchIdentityKey	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeIdentity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeIdentity]
	@ObjectId uniqueidentifier,
	@IdentityKey nvarchar(400),
	@IdentityData nvarchar(max) = NULL
AS
BEGIN
	SET NOCOUNT ON
	
	IF EXISTS (SELECT * FROM [IdentityServerPolicy].[Scopes] WHERE ScopeId = @ObjectId)
		BEGIN
			INSERT [IdentityServerPolicy].[ScopeIdentities](ScopeId, IdentityKey, IdentityData)
			VALUES (@ObjectId, @IdentityKey, @IdentityData)			
		END
	ELSE
		RAISERROR(''Could not find Scope'',10,1)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeIdentity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeIdentity]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT s.IdentityData FROM [IdentityServerPolicy].[ScopeIdentities] s
	WHERE s.ScopeId = @ObjectId
END
'
END
GO		

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeIdentity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeIdentity]
	@ObjectId uniqueidentifier,
	@IdentityKey nvarchar(400) = NULL,
	@IdentityData nvarchar(max) = NULL,
	@debug bit = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @sql nvarchar(max),
	   @paramlist nvarchar(max)
	
   SELECT @sql = 
   ''DELETE FROM [IdentityServerPolicy].[ScopeIdentities]
	 WHERE ScopeId = @ObjectId''
   
   IF @IdentityKey IS NOT NULL
	SELECT @sql = @sql + '' AND IdentityKey = @IdentityKey''
	
   IF @IdentityData IS NOT NULL
	SELECT @sql = @sql + '' AND IdentityData = @IdentityData''
   
   SELECT @paramlist = ''@ObjectId		uniqueidentifier,
						 @IdentityKey 		nvarchar(400),
						 @IdentityData 		nvarchar(max)''

   IF @debug = 1
		PRINT @sql
   
   EXEC dbo.sp_executesql @sql, @paramlist, @ObjectId, @IdentityKey, @IdentityData
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeSigningCertificate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeSigningCertificate]
	@ObjectId uniqueidentifier,
	@CertificateKey nvarchar(400) = NULL,
	@CertificateData nvarchar(max) = NULL
AS
BEGIN
	SET NOCOUNT ON
		
	SELECT * FROM [IdentityServerPolicy].[Scopes] WHERE ScopeId = @ObjectId
	
	INSERT [IdentityServerPolicy].[ScopeSigningCertificates](ScopeId, CertificateKey, CertificateData)
	VALUES (@ObjectId, @CertificateKey, @CertificateData)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeSigningCertificate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeSigningCertificate]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT s.CertificateData FROM [IdentityServerPolicy].[ScopeSigningCertificates] s
	WHERE s.ScopeId = @ObjectId
END
'
END
GO		

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeSigningCertificate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeSigningCertificate]
	@ObjectId uniqueidentifier,
	@CertificateKey nvarchar(400) = NULL,
	@CertificateData nvarchar(max) = NULL,
	@debug bit = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @sql nvarchar(max),
	   @paramlist nvarchar(max)

   SELECT @sql = 
   ''DELETE FROM [IdentityServerPolicy].[ScopeSigningCertificates]
	 WHERE ScopeId = @ObjectId''
	
   IF @CertificateKey IS NOT NULL
	SELECT @sql = @sql + '' AND CertificateKey = @CertificateKey''
	
   IF @CertificateData IS NOT NULL
	SELECT @sql = @sql + '' AND CertificateData = @CertificateData''
   
   SELECT @paramlist = ''@ObjectId		uniqueidentifier,
						 @CertificateKey 		nvarchar(400),
						 @CertificateData 		nvarchar(max)''

   IF @debug = 1
		PRINT @sql
   
   EXEC dbo.sp_executesql @sql, @paramlist, @ObjectId, @CertificateKey, @CertificateData
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeContactInfoAddress]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeContactInfoAddress]
	@ObjectId uniqueidentifier,
	@ContactInfoAddress nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT [IdentityServerPolicy].[ScopeContactInfoAddresses](ScopeId, ContactInfoAddress)
	VALUES (@ObjectId, @ContactInfoAddress)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeContactInfoAddress]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeContactInfoAddress]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT s.ContactInfoAddress FROM [IdentityServerPolicy].[ScopeContactInfoAddresses] s
	WHERE s.ScopeId = @ObjectId
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeContactInfoAddress]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeContactInfoAddress]
	@ObjectId uniqueidentifier,
	@ContactInfoAddress nvarchar(max)  = NULL
AS
BEGIN
	SET NOCOUNT ON
	IF @ContactInfoAddress IS NOT NULL
	BEGIN
		DELETE FROM [IdentityServerPolicy].[ScopeContactInfoAddresses] WHERE ScopeId = @ObjectId AND ContactInfoAddress = @ContactInfoAddress
	END
	ELSE
	BEGIN
		DELETE FROM [IdentityServerPolicy].[ScopeContactInfoAddresses] WHERE ScopeId = @ObjectId
	END	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopePolicy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopePolicy]
	@ObjectId uniqueidentifier,
	@PolicyData nvarchar(max) = NULL,
	@PolicyType nvarchar(400),
	@PolicyUsage int
AS
BEGIN
	SET NOCOUNT ON
	DECLARE	@policyid uniqueidentifier
	DECLARE @outputPolicyTbl TABLE (ObjectId uniqueidentifier NOT NULL)
		
	BEGIN TRAN
	
	INSERT [IdentityServerPolicy].[Policies](PolicyData, PolicyType, PolicyUsage)
	OUTPUT Inserted.PolicyId INTO @outputPolicyTbl
	VALUES (@PolicyData, @PolicyType, @PolicyUsage)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of policy entry failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END

	SELECT @policyid = ObjectId FROM @outputPolicyTbl
	
	INSERT [IdentityServerPolicy].[ScopePolicies](ScopeId, PolicyId)
	VALUES (@ObjectId, @policyid)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of scope policy mapping failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	COMMIT TRAN

	-- if an error is encountered, skip all the way to this point
	end_of_batch:
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopePolicies]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopePolicies]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT p.PolicyData, p.PolicyType, p.PolicyUsage FROM [IdentityServerPolicy].[Policies] p
	JOIN [IdentityServerPolicy].[ScopePolicies] s ON p.PolicyId = s.PolicyId
	WHERE s.ScopeId = @ObjectId
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopePolicy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopePolicy]
	@ObjectId uniqueidentifier,
	@PolicyData nvarchar(max) = NULL,
	@PolicyType nvarchar(400),
	@PolicyUsage int
AS
BEGIN
	SET NOCOUNT ON
		
	-- Delete the ScopePolicies entry, the corresponding Policy entry gets deleted by a trigger
	IF @PolicyData IS NOT NULL
		DELETE FROM [IdentityServerPolicy].[ScopePolicies]
		FROM [IdentityServerPolicy].[Policies] p
		JOIN [IdentityServerPolicy].[ScopePolicies] s ON s.PolicyId = p.PolicyID 
		WHERE s.ScopeId = @ObjectId 
			AND p.PolicyType = @PolicyType AND p.PolicyUsage = @PolicyUsage
			AND p.PolicyData = @PolicyData
	ELSE
		DELETE FROM [IdentityServerPolicy].[ScopePolicies]
		FROM [IdentityServerPolicy].[Policies] p
		JOIN [IdentityServerPolicy].[ScopePolicies] s ON s.PolicyId = p.PolicyID 
		WHERE s.ScopeId = @ObjectId 
			AND p.PolicyType = @PolicyType AND p.PolicyUsage = @PolicyUsage
			AND p.PolicyData IS NULL	
END
'
END
GO
	  
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[CreateClaimDescriptor]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[CreateClaimDescriptor]
	@ObjectId uniqueidentifier OUTPUT,
	@ClaimType	nvarchar(400),
	@ClaimLanguage	nvarchar(10),
	@ClaimName		nvarchar(400),
	@ClaimDescription	nvarchar(500) = NULL,
	@PublishingOptions int = 0,
	@RemoteObjectId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @claimid uniqueidentifier
	DECLARE @outputClaimTypeTbl TABLE (ObjectId uniqueidentifier NOT NULL)
 	DECLARE @outputClaimDescriptorTbl TABLE (ObjectId uniqueidentifier NOT NULL)
 	 			
	BEGIN TRAN

	SELECT @claimid = ClaimId FROM [IdentityServerPolicy].[ClaimTypes] WHERE ClaimType = @ClaimType
	IF @claimid IS NULL
	BEGIN
		INSERT [IdentityServerPolicy].[ClaimTypes](ClaimType, PublishingOptions) 
		OUTPUT INSERTED.ClaimId INTO @outputClaimTypeTbl
		VALUES (@ClaimType, @PublishingOptions )
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Insertion of claimtype entry failed'',10,1)
			ROLLBACK TRAN
			GOTO end_of_batch
		END
		SELECT @claimid = ObjectId FROM @outputClaimTypeTbl 
	END
	
	INSERT [IdentityServerPolicy].[ClaimDescriptors](ClaimId, Language, Name, Description)
	OUTPUT INSERTED.ClaimDescriptorId INTO @outputClaimDescriptorTbl
	VALUES (@claimid, @ClaimLanguage, @ClaimName, @ClaimDescription)	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of claimdescriptor entry failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END 
	SELECT @ObjectId = ObjectId FROM @outputClaimDescriptorTbl 

	IF @RemoteObjectId IS NOT NULL	
	BEGIN
		UPDATE [IdentityServerPolicy].[ClaimDescriptors] SET ClaimDescriptorId = @RemoteObjectId WHERE ClaimDescriptorId = @ObjectId
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Creation of claimdescriptor entry failed'',10,1)
			ROLLBACK TRAN
			GOTO end_of_batch
		END
		SET @ObjectId = @RemoteObjectId
	END	
	
	COMMIT TRAN

	-- if an error is encountered, skip all the way to this point
	end_of_batch:			
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetClaimDescriptors]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetClaimDescriptors]
	@ClaimDescriptorId uniqueidentifier = NULL,
	@ClaimType	nvarchar(400) = NULL,
	@ClaimLanguage	nvarchar(10) = NULL,
	@debug bit = 0
AS
BEGIN
   SET NOCOUNT ON;
   DECLARE	@sql	nvarchar(max),
			@paramlist nvarchar(max)
   
   SELECT @sql = 
   ''SELECT cd.ClaimDescriptorId, ct.ClaimType, cd.Name, cd.Language, cd.Description, ct.PublishingOptions
	FROM [IdentityServerPolicy].[ClaimDescriptors] cd
	JOIN [IdentityServerPolicy].[ClaimTypes] ct ON cd.ClaimId = ct.ClaimId 
	WHERE  1 = 1''
   
	IF @ClaimDescriptorId IS NOT NULL
 		SELECT @sql = @sql + '' AND cd.ClaimDescriptorId = @ClaimDescriptorId''
	IF @ClaimType IS NOT NULL
 		SELECT @sql = @sql + '' AND ct.ClaimType = @Type''
	IF @ClaimLanguage IS NOT NULL
 		SELECT @sql = @sql + '' AND cd.Language = @Language''
	
	SELECT @paramlist = ''@ClaimDescriptorId uniqueidentifier,
						  @Type 		nvarchar(400),
						  @Language		nvarchar(10)''
	

   IF @debug = 1
		PRINT @sql

   EXEC dbo.sp_executesql @sql, @paramlist, @ClaimDescriptorId, @ClaimType, @ClaimLanguage
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateClaimDescriptor]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateClaimDescriptor]
	@ObjectId uniqueidentifier,
	@ClaimType	nvarchar(400) = NULL,
	@ClaimLanguage	nvarchar(10) = NULL,
	@ClaimName nvarchar(400) = NULL,
	@ClaimDescription nvarchar(500) = NULL,
	@PublishingOptions int = NULL,
	@NullClaimDescription bit = 0,
	@NullPublishingOptions bit = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @claimid uniqueidentifier
	
	BEGIN TRAN

	IF @ClaimType IS NOT NULL
	BEGIN
		SET @claimid = NULL
		SELECT @claimid = ct.ClaimId
		FROM [IdentityServerPolicy].[ClaimTypes] ct
		WHERE ct.ClaimType = @ClaimType		
		IF @claimid IS NOT NULL
		BEGIN
			-- update the claim descriptor entry to refer to the new existing claimtype
			UPDATE [IdentityServerPolicy].[ClaimDescriptors]
			SET ClaimId = @claimid
			WHERE ClaimDescriptorId = @ObjectId
			IF @@ERROR <> 0
			BEGIN
				RAISERROR(''Update of claimdescriptor claimtype failed'',10,1)
				ROLLBACK TRAN
				GOTO end_of_batch
			END 
		END
		ELSE
		BEGIN
			-- no claimtype entry exists, so change the currently referenced entry
			UPDATE [IdentityServerPolicy].[ClaimTypes]
			SET ClaimType = @ClaimType
			WHERE ClaimId = (SELECT cd.ClaimId FROM [IdentityServerPolicy].[ClaimDescriptors] cd WHERE cd.ClaimDescriptorId = @ObjectId)
			IF @@ERROR <> 0
			BEGIN
				RAISERROR(''Update of claimtype entry failed'',10,1)
				ROLLBACK TRAN
				GOTO end_of_batch
			END 	
		END
	END

	UPDATE [IdentityServerPolicy].[ClaimDescriptors]
	SET Language = CASE WHEN @ClaimLanguage IS NULL THEN Language ELSE @ClaimLanguage END,
		Name = CASE WHEN @ClaimName IS NULL THEN Name ELSE @ClaimName END,
		Description = CASE WHEN @ClaimDescription IS NULL THEN Description ELSE @ClaimDescription END 
	WHERE ClaimDescriptorId = @ObjectId
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of claimdescriptor fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END 

	UPDATE [IdentityServerPolicy].[ClaimTypes]
	SET PublishingOptions = CASE WHEN @PublishingOptions IS NULL THEN PublishingOptions ELSE @PublishingOptions END
	WHERE ClaimId = (SELECT cd.ClaimId FROM [IdentityServerPolicy].[ClaimDescriptors] cd WHERE cd.ClaimDescriptorId = @ObjectId)
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of claim type fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END 

	IF @NullClaimDescription = 1
	BEGIN
		UPDATE [IdentityServerPolicy].[ClaimDescriptors]
			SET Description = NULL
		WHERE ClaimDescriptorId = @ObjectId
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Reset of claimdescriptor fields failed'',10,1)
			ROLLBACK TRAN
			GOTO end_of_batch
		END
	END

	IF @NullPublishingOptions = 1
	BEGIN
		UPDATE [IdentityServerPolicy].[ClaimTypes]
			SET PublishingOptions = NULL
		WHERE ClaimId = (SELECT cd.ClaimId FROM [IdentityServerPolicy].[ClaimDescriptors] cd WHERE cd.ClaimDescriptorId = @ObjectId)
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Reset of claim type fields failed'',10,1)
			ROLLBACK TRAN
			GOTO end_of_batch
		END
	END
	
	COMMIT TRAN

	-- if an error is encountered, skip all the way to this point
	end_of_batch:			
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[DeleteClaimDescriptors]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[DeleteClaimDescriptors]
	@ObjectId uniqueidentifier = NULL,
	@ClaimType	nvarchar(400) = NULL,
	@ClaimLanguage	nvarchar(10) = NULL,
	@debug bit = 0
AS
BEGIN
   SET NOCOUNT ON;
   DECLARE	@sql nvarchar(max), 
			@paramlist nvarchar(max)
   
   SELECT @sql = 
   ''DELETE FROM [IdentityServerPolicy].[ClaimDescriptors]
			FROM [IdentityServerPolicy].[ClaimTypes] ct
			JOIN [IdentityServerPolicy].[ClaimDescriptors] cd ON cd.ClaimId = ct.ClaimId
	 WHERE  1 = 1''
   
	IF @ObjectId IS NOT NULL
 		SELECT @sql = @sql + '' AND cd.ClaimDescriptorId = @ObjectId''
	IF @ClaimType IS NOT NULL
 		SELECT @sql = @sql + '' AND ct.ClaimType = @Type''
	IF @ClaimLanguage IS NOT NULL
 		SELECT @sql = @sql + '' AND cd.Language = @Language''
	
	SELECT @paramlist = ''@ObjectId uniqueidentifier,
						  @Type 		nvarchar(400),
						  @Language		nvarchar(10)''
	

   IF @debug = 1
		PRINT @sql
  
   EXEC dbo.sp_executesql @sql, @paramlist, @ObjectId, @ClaimType, @ClaimLanguage
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetMetadataSources]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetMetadataSources]
	@ObjectId uniqueidentifier = NULL,
	@PollPublishedPolicyOption bit = NULL,
	@debug bit = 0
AS
BEGIN
   SET NOCOUNT ON;
   DECLARE	@sql	nvarchar(max),
			@paramlist nvarchar(max)
   
   SELECT @sql = 
   ''SELECT ObjectId, PrincipalObjectType, 
				PollPublishedPolicyOption, PublishedPolicyContents, PublishedPolicyUrl,
				LastPublishedPolicyCheckSuccessful, LastPublishedPolicyCheckTime
	FROM [IdentityServerPolicy].[MetadataSources] WHERE  1 = 1''
   
	IF @ObjectId IS NOT NULL
 		SELECT @sql = @sql + '' AND ObjectId = @ObjectId''
	IF @PollPublishedPolicyOption IS NOT NULL
 		SELECT @sql = @sql + '' AND PollPublishedPolicyOption = @PollPublishedPolicyOption''
	
	SELECT @paramlist = ''@ObjectId uniqueidentifier,
						  @PollPublishedPolicyOption bit''
	
	IF @debug = 1
		PRINT @sql

   EXEC dbo.sp_executesql @sql, @paramlist, @ObjectId, @PollPublishedPolicyOption
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateMetadataSource]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateMetadataSource]
	@ObjectId uniqueidentifier,	
	@PollPublishedPolicyOption bit = NULL,
	@PublishedPolicyContents nvarchar(max) = NULL,
	@PublishedPolicyUrl nvarchar(400) = NULL,
	@LastPublishedPolicyCheckSuccessful int = NULL,
	@LastPublishedPolicyCheckTime nvarchar(50) = NULL,
	@NullPollPublishedPolicyOption bit = 0,
	@NullPublishedPolicyContents bit = 0,
	@NullPublishedPolicyUrl bit = 0,
	@NullLastPublishedPolicyCheckSuccessful bit = 0,
	@NullLastPublishedPolicyCheckTime bit = 0
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @objectType nvarchar(255)
	
	BEGIN TRAN
	
	UPDATE [IdentityServerPolicy].[MetadataSources]
		SET PollPublishedPolicyOption = CASE WHEN @PollPublishedPolicyOption IS NULL THEN PollPublishedPolicyOption ELSE @PollPublishedPolicyOption END,
			PublishedPolicyContents = CASE WHEN @PublishedPolicyContents IS NULL THEN PublishedPolicyContents ELSE @PublishedPolicyContents END,
			PublishedPolicyUrl = CASE WHEN @PublishedPolicyUrl IS NULL THEN PublishedPolicyUrl ELSE @PublishedPolicyUrl END,
			LastPublishedPolicyCheckSuccessful = CASE WHEN @LastPublishedPolicyCheckSuccessful IS NULL THEN LastPublishedPolicyCheckSuccessful ELSE @LastPublishedPolicyCheckSuccessful END,
			LastPublishedPolicyCheckTime = CASE WHEN @LastPublishedPolicyCheckTime IS NULL THEN LastPublishedPolicyCheckTime ELSE @LastPublishedPolicyCheckTime END
	WHERE ObjectId = @ObjectId
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Update of metadata source fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	UPDATE [IdentityServerPolicy].[MetadataSources]
		SET PollPublishedPolicyOption = CASE WHEN @NullPollPublishedPolicyOption = 1 THEN NULL ELSE PollPublishedPolicyOption END,
			PublishedPolicyContents = CASE WHEN @NullPublishedPolicyContents = 1 THEN NULL ELSE PublishedPolicyContents END,
			PublishedPolicyUrl = CASE WHEN @NullPublishedPolicyUrl = 1 THEN NULL ELSE PublishedPolicyUrl END,
			LastPublishedPolicyCheckSuccessful = CASE WHEN @NullLastPublishedPolicyCheckSuccessful = 1 THEN NULL ELSE LastPublishedPolicyCheckSuccessful END,
			LastPublishedPolicyCheckTime = CASE WHEN @NullLastPublishedPolicyCheckTime = 1 THEN NULL ELSE LastPublishedPolicyCheckTime END
	WHERE ObjectId = @ObjectId
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Reset of metadata source fields failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	SELECT @objectType = LTRIM(RTRIM(PrincipalObjectType)) FROM  [IdentityServerPolicy].[MetadataSources] WHERE ObjectId = @ObjectId
	EXEC  [IdentityServerPolicy].[IncrementServiceStateSummarySerialNumber] @objectType
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Incrementing principal object type serial number failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END   
	
	COMMIT TRAN

	-- if an error is encountered, skip all the way to this point
	end_of_batch:		
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthorityArtifactResolutionService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthorityArtifactResolutionService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL,
	@EndpointIndex int,
	@IsDefault bit = NULL
AS
BEGIN
	SET NOCOUNT ON
	INSERT [IdentityServerPolicy].[AuthorityArtifactResolutionServices](AuthorityId, Binding, Location, ResponseLocation, EndpointIndex, IsDefault)
	VALUES (@ObjectId, @Binding, @Location, @ResponseLocation, @EndpointIndex, @IsDefault)
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthorityArtifactResolutionService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthorityArtifactResolutionService]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.Binding, a.Location, a.ResponseLocation, a.EndpointIndex, a.IsDefault FROM [IdentityServerPolicy].[AuthorityArtifactResolutionServices] a
	WHERE a.AuthorityId = @ObjectId
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthorityArtifactResolutionService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthorityArtifactResolutionService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL,
	@EndpointIndex int,
	@IsDefault bit = NULL
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM [IdentityServerPolicy].[AuthorityArtifactResolutionServices] 
		WHERE AuthorityId = @ObjectId AND EndpointIndex = @EndpointIndex 
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthoritySingleLogoutService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthoritySingleLogoutService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL
AS
BEGIN
	SET NOCOUNT ON
	INSERT [IdentityServerPolicy].[AuthoritySamlEndpoints](AuthorityId, Binding, Location, ResponseLocation, EndpointType)
	VALUES (@ObjectId, @Binding, @Location, @ResponseLocation, 0)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthoritySingleLogoutService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthoritySingleLogoutService]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.Binding, a.Location, a.ResponseLocation FROM [IdentityServerPolicy].[AuthoritySamlEndpoints] a
	WHERE a.AuthorityId = @ObjectId AND a.EndpointType = 0
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthoritySingleLogoutService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthoritySingleLogoutService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL
AS
BEGIN
	SET NOCOUNT ON
	IF @ResponseLocation IS NOT NULL
		DELETE FROM [IdentityServerPolicy].[AuthoritySamlEndpoints] WHERE AuthorityId = @ObjectId 
				AND Binding = @Binding AND Location = @Location AND EndpointType = 0 
				AND ResponseLocation = @ResponseLocation
	ELSE
		DELETE FROM [IdentityServerPolicy].[AuthoritySamlEndpoints] WHERE AuthorityId = @ObjectId 
				AND Binding = @Binding AND Location = @Location AND EndpointType = 0 
				AND ResponseLocation IS NULL				
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthoritySingleSignOnService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthoritySingleSignOnService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL
AS
BEGIN
	SET NOCOUNT ON		
	INSERT [IdentityServerPolicy].[AuthoritySamlEndpoints](AuthorityId, Binding, Location, ResponseLocation, EndpointType)
	VALUES (@ObjectId, @Binding, @Location, @ResponseLocation, 1)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthoritySingleSignOnService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthoritySingleSignOnService]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.Binding, a.Location, a.ResponseLocation FROM [IdentityServerPolicy].[AuthoritySamlEndpoints] a
	WHERE a.AuthorityId = @ObjectId AND a.EndpointType = 1
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthoritySingleSignOnService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthoritySingleSignOnService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL
AS
BEGIN
	SET NOCOUNT ON
	IF @ResponseLocation IS NOT NULL
		DELETE FROM [IdentityServerPolicy].[AuthoritySamlEndpoints] WHERE AuthorityId = @ObjectId 
				AND Binding = @Binding AND Location = @Location AND EndpointType = 1 
				AND ResponseLocation = @ResponseLocation
	ELSE
		DELETE FROM [IdentityServerPolicy].[AuthoritySamlEndpoints] WHERE AuthorityId = @ObjectId 
				AND Binding = @Binding AND Location = @Location AND EndpointType = 1 
				AND ResponseLocation IS NULL
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeSingleLogoutService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeSingleLogoutService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL
AS
BEGIN
	SET NOCOUNT ON
	INSERT [IdentityServerPolicy].[ScopeSamlEndpoints](ScopeId, Binding, Location, ResponseLocation, EndpointType)
	VALUES (@ObjectId, @Binding, @Location, @ResponseLocation, 0)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeSingleLogoutService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeSingleLogoutService]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT s.Binding, s.Location, s.ResponseLocation FROM [IdentityServerPolicy].[ScopeSamlEndpoints] s
	WHERE s.ScopeId = @ObjectId AND s.EndpointType = 0
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeSingleLogoutService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeSingleLogoutService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL
AS
BEGIN
	SET NOCOUNT ON
	IF @ResponseLocation IS NOT NULL
		DELETE FROM [IdentityServerPolicy].[ScopeSamlEndpoints] WHERE ScopeId = @ObjectId 
				AND Binding = @Binding AND Location = @Location AND EndpointType = 0
				AND ResponseLocation = @ResponseLocation
	ELSE
		DELETE FROM [IdentityServerPolicy].[ScopeSamlEndpoints] WHERE ScopeId = @ObjectId 
				AND Binding = @Binding AND Location = @Location AND EndpointType = 0
				AND ResponseLocation IS NULL
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeAssertionConsumerService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeAssertionConsumerService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL,
	@EndpointIndex int,
	@IsDefault bit = NULL
AS
BEGIN
	SET NOCOUNT ON	
	INSERT [IdentityServerPolicy].[ScopeAssertionConsumerServices](ScopeId, Binding, Location, ResponseLocation, EndpointIndex, IsDefault)
	VALUES (@ObjectId, @Binding, @Location, @ResponseLocation, @EndpointIndex, @IsDefault)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeAssertionConsumerService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeAssertionConsumerService]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT s.Binding, s.Location, s.ResponseLocation, s.EndpointIndex, s.IsDefault FROM [IdentityServerPolicy].[ScopeAssertionConsumerServices] s
	WHERE s.ScopeId = @ObjectId
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeAssertionConsumerService]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeAssertionConsumerService]
	@ObjectId uniqueidentifier,
	@Binding nvarchar(400),
	@Location nvarchar(400),
	@ResponseLocation nvarchar(400) = NULL,
	@EndpointIndex int,
	@IsDefault bit = NULL
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [IdentityServerPolicy].[ScopeAssertionConsumerServices]
		WHERE ScopeId = @ObjectId  AND EndpointIndex = @EndpointIndex
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthorityClaimType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthorityClaimType]
	@ObjectId uniqueidentifier,
	@ClaimType nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT [IdentityServerPolicy].[AuthorityClaimTypes](AuthorityId, ClaimType)
	VALUES (@ObjectId, @ClaimType)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthorityClaimType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthorityClaimType]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.ClaimType FROM [IdentityServerPolicy].[AuthorityClaimTypes] a
	WHERE a.AuthorityId = @ObjectId
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthorityClaimType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthorityClaimType]
	@ObjectId uniqueidentifier,
	@ClaimType nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [IdentityServerPolicy].[AuthorityClaimTypes] WHERE AuthorityId = @ObjectId 
				AND ClaimType = @ClaimType 	
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeClaimTypeOptional]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeClaimTypeOptional]
	@ObjectId uniqueidentifier,
	@ClaimType nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT [IdentityServerPolicy].[ScopeClaimTypes](ScopeId, ClaimType, Required)
	VALUES (@ObjectId, @ClaimType, 0)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeClaimTypeOptional]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeClaimTypeOptional]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.ClaimType FROM [IdentityServerPolicy].[ScopeClaimTypes] a
	WHERE a.ScopeId = @ObjectId AND a.Required = 0
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeClaimTypeOptional]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeClaimTypeOptional]
	@ObjectId uniqueidentifier,
	@ClaimType nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [IdentityServerPolicy].[ScopeClaimTypes] 
	WHERE ScopeId = @ObjectId AND ClaimType = @ClaimType AND Required = 0	
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeClaimTypeRequired]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeClaimTypeRequired]
	@ObjectId uniqueidentifier,
	@ClaimType nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT [IdentityServerPolicy].[ScopeClaimTypes](ScopeId, ClaimType, Required)
	VALUES (@ObjectId, @ClaimType, 1)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeClaimTypeRequired]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeClaimTypeRequired]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT a.ClaimType FROM [IdentityServerPolicy].[ScopeClaimTypes] a
	WHERE a.ScopeId = @ObjectId AND a.Required = 1
END
'
END
GO	  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeClaimTypeRequired]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeClaimTypeRequired]
	@ObjectId uniqueidentifier,
	@ClaimType nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [IdentityServerPolicy].[ScopeClaimTypes] 
	WHERE ScopeId = @ObjectId AND ClaimType = @ClaimType AND Required = 1	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[CreateServiceSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[CreateServiceSettings]
	@ObjectId uniqueidentifier OUTPUT,
	@ServiceSettingsData nvarchar(max),
	@ServiceSettingsVersion bigint = 0,
	@Name nvarchar(400) = NULL,
	@LastUpdateTime nvarchar(50) = NULL,
	@RemoteObjectId uniqueidentifier = NULL
AS
BEGIN
	DECLARE @tempId uniqueidentifier   
	SET @tempId = @RemoteObjectId	   
	   
	BEGIN TRAN
	IF NOT EXISTS (SELECT TOP 1 ServiceSettingsData FROM [IdentityServerPolicy].[ServiceSettings])
	BEGIN
		IF @tempId IS NULL
		BEGIN
			SET @tempId = NEWID()
		END
		INSERT INTO [IdentityServerPolicy].[ServiceSettings]
		(
		 ServiceSettingId,
		 ServiceSettingsData,
		 LastUpdateTime,
		 ServiceSettingsVersion
		)
		VALUES
		(
		 @tempId,
		 @ServiceSettingsData,
		 getutcdate(),
		 @ServiceSettingsVersion
		)
	END
	ELSE
	BEGIN
		IF @tempId IS NULL
		BEGIN
		   SELECT @tempId = ServiceSettingId FROM [IdentityServerPolicy].[ServiceSettings]
		END
		UPDATE [IdentityServerPolicy].[ServiceSettings]
			SET ServiceSettingId = @tempId,
				ServiceSettingsData = @ServiceSettingsData,
				LastUpdateTime = getutcdate(),
				ServiceSettingsVersion = @ServiceSettingsVersion
	END
	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Creating service settings failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END   

	SET @ObjectId = @tempId

	COMMIT TRAN

	-- if an error is encountered, skip all the way to this point
	end_of_batch:	
END
'
END
GO	

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetServiceSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetServiceSettings]
@ObjectId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SELECT ServiceSettingId, N''ConfigurationSettings'', ServiceSettingsData, CONVERT(nvarchar, LastUpdateTime), ServiceSettingsVersion
	FROM [IdentityServerPolicy].[ServiceSettings]
END
'
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[UpdateServiceSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[UpdateServiceSettings]
	@ObjectId uniqueidentifier,
	@ServiceSettingsData nvarchar(max) = NULL,
	@ServiceSettingsVersion bigint = 0
AS
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @returnCode INT
	-- set success return code by default
	SET @returnCode = 0
	BEGIN TRAN
	
	-- we need to take the exclusive lock even for read and also hold the lock for the 
	-- duration of the transaction so that a parallel update request does not read a stale version
	DECLARE @currentServerVersion BIGINT
	SELECT @currentServerVersion = ServiceSettingsVersion
	FROM [IdentityServerPolicy].[ServiceSettings] WITH (UPDLOCK, HOLDLOCK)
	WHERE ServiceSettingId = @ObjectId
	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Locking service settings failed'',10,1)
		ROLLBACK TRAN
		GOTO end_of_batch
	END
	
	IF @currentServerVersion = @ServiceSettingsVersion
	BEGIN
		-- client is up to date
		UPDATE [IdentityServerPolicy].[ServiceSettings]
		SET ServiceSettingsData = CASE WHEN @ServiceSettingsData IS NULL THEN ServiceSettingsData ELSE @ServiceSettingsData END,
			LastUpdateTime = getutcdate(),
			ServiceSettingsVersion = @currentServerVersion + 1
		WHERE ServiceSettingId = @ObjectId
		
		IF @@ERROR <> 0
		BEGIN
			RAISERROR(''Updating service settings failed'',10,1)
			ROLLBACK TRAN
			GOTO end_of_batch
		END
	END
	ELSE
	BEGIN
		-- Cannot update since client started with an incorrect version
		-- If changing the return code, also change the SqlReturnCodeUtility
		SET @returnCode = 1001
	END   

	COMMIT TRAN
	
	-- if an error is encountered, skip all the way to this point
	end_of_batch:
	
	RETURN @returnCode
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[DeleteServiceSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[DeleteServiceSettings]
@ObjectId uniqueIdentifier = NULL
AS
BEGIN	
	DELETE FROM [IdentityServerPolicy].[ServiceSettings]	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[QueryServiceStateSummaryModification]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[QueryServiceStateSummaryModification]
AS
BEGIN
	SELECT ServiceObjectType, SerialNumber, SchemaVersionNumber, LastUpdateTime FROM [IdentityServerPolicy].[ServiceStateSummary]
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddLeasedTask]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddLeasedTask]
	@TaskName	nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	INSERT [IdentityServerPolicy].[LeasedTasks](TaskName, LastLease, LeaseExpiration)
	VALUES (@TaskName, NULL, getutcdate())	
	IF @@ERROR <> 0
	BEGIN
		RAISERROR(''Insertion of leased task entry failed'',10,1)
	END 
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[ResetLeasedTask]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[ResetLeasedTask]
	@TaskName nvarchar(400),
	@DurationInMinutes int
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE [IdentityServerPolicy].[LeasedTasks] 
		SET LeaseExpiration = CASE WHEN LastLease IS NULL THEN getutcdate() ELSE dateadd( minute, @DurationInMinutes, LastLease ) END
	WHERE ( TaskName = @TaskName ) 
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[TryAcquireLease]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[TryAcquireLease]
	@LeaseAcquired bit OUTPUT,
	@TaskName nvarchar(400),
	@DurationInMinutes int
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @leasedTbl TABLE (TaskName nvarchar(400) NOT NULL)
	UPDATE [IdentityServerPolicy].[LeasedTasks] 
		SET LeaseExpiration = dateadd( minute, @DurationInMinutes, getutcdate() ),
			LastLease = getutcdate()
		OUTPUT INSERTED.TaskName INTO @leasedTbl
	WHERE ( TaskName = @TaskName ) AND ( LeaseExpiration < getutcdate() )
	IF EXISTS ( SELECT TaskName FROM @leasedTbl )
		SET @LeaseAcquired = 1
	ELSE
		SET @LeaseAcquired = 0
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveLeasedTask]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveLeasedTask]
	@TaskName	nvarchar(400) = NULL,
	@debug bit = 0
AS
BEGIN
   SET NOCOUNT ON
   DECLARE	@sql nvarchar(max), 
	  @paramlist nvarchar(max)
   
   SELECT @sql = 
   ''DELETE FROM [IdentityServerPolicy].[LeasedTasks] WHERE  1 = 1''
   
   IF @TaskName IS NOT NULL
   SELECT @sql = @sql + '' AND TaskName = @TaskName''
	
   SELECT @paramlist = ''@TaskName nvarchar(400)''
	
   IF @debug = 1
		PRINT @sql
  
   EXEC dbo.sp_executesql @sql, @paramlist, @TaskName
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddClaimDescriptorExtensibleProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddClaimDescriptorExtensibleProperty]
	@ObjectId uniqueidentifier,
	@PropertyName nvarchar(400),
	@PropertyValue nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT [IdentityServerPolicy].[ClaimDescriptorExtensibleProperties](ClaimDescriptorId, PropertyName, PropertyValue)
	VALUES (@ObjectId, @PropertyName, @PropertyValue)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveClaimDescriptorExtensibleProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveClaimDescriptorExtensibleProperty]
	@ObjectId uniqueidentifier,
	@PropertyName nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [IdentityServerPolicy].[ClaimDescriptorExtensibleProperties]
	WHERE ClaimDescriptorId = @ObjectId AND 
		PropertyName = @PropertyName
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetClaimDescriptorExtensibleProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetClaimDescriptorExtensibleProperties]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT PropertyName, PropertyValue 
	FROM [IdentityServerPolicy].[ClaimDescriptorExtensibleProperties]
	WHERE ClaimDescriptorId = @ObjectId
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddScopeExtensibleProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddScopeExtensibleProperty]
	@ObjectId uniqueidentifier,
	@PropertyName nvarchar(400),
	@PropertyValue nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT [IdentityServerPolicy].[ScopeExtensibleProperties](ScopeId, PropertyName, PropertyValue)
	VALUES (@ObjectId, @PropertyName, @PropertyValue)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveScopeExtensibleProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveScopeExtensibleProperty]
	@ObjectId uniqueidentifier,
	@PropertyName nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [IdentityServerPolicy].[ScopeExtensibleProperties]
	WHERE ScopeId = @ObjectId AND 
		PropertyName = @PropertyName
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetScopeExtensibleProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetScopeExtensibleProperties]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT PropertyName, PropertyValue 
	FROM [IdentityServerPolicy].[ScopeExtensibleProperties]
	WHERE ScopeId = @ObjectId
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[AddAuthorityExtensibleProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[AddAuthorityExtensibleProperty]
	@ObjectId uniqueidentifier,
	@PropertyName nvarchar(400),
	@PropertyValue nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT [IdentityServerPolicy].[AuthorityExtensibleProperties](AuthorityId, PropertyName, PropertyValue)
	VALUES (@ObjectId, @PropertyName, @PropertyValue)	
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[RemoveAuthorityExtensibleProperty]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[RemoveAuthorityExtensibleProperty]
	@ObjectId uniqueidentifier,
	@PropertyName nvarchar(400)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [IdentityServerPolicy].[AuthorityExtensibleProperties]
	WHERE AuthorityId = @ObjectId AND 
		PropertyName = @PropertyName
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[GetAuthorityExtensibleProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [IdentityServerPolicy].[GetAuthorityExtensibleProperties]
	@ObjectId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT PropertyName, PropertyValue 
	FROM [IdentityServerPolicy].[AuthorityExtensibleProperties]
	WHERE AuthorityId = @ObjectId
END
'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[CreateDefaultObjects]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [IdentityServerPolicy].[CreateDefaultObjects]
	AS
	BEGIN
		DECLARE @id uniqueidentifier;

		EXEC IdentityServerPolicy.AddServiceStateSummary
		@ServiceObjectType = ''IssuanceAuthority'',
		@SerialNumber = 0,
		@SchemaVersionNumber = 1

		EXEC IdentityServerPolicy.AddServiceStateSummary
		@ServiceObjectType = ''IssuanceScope'',
		@SerialNumber = 0,
		@SchemaVersionNumber = 1

		EXEC IdentityServerPolicy.AddServiceStateSummary
		@ServiceObjectType = ''IssuanceClaimDescriptor'',
		@SerialNumber = 0,
		@SchemaVersionNumber = 1

		EXEC IdentityServerPolicy.AddServiceStateSummary
		@ServiceObjectType = ''ServiceSettings'',
		@SerialNumber = 0,
		@SchemaVersionNumber = 1

		EXEC IdentityServerPolicy.AddSyncProperties
		@PropertyName = ''Role'',
		@PropertyValue = ''PrimaryComputer''

		EXEC IdentityServerPolicy.AddSyncProperties
		@PropertyName = ''PrimaryComputerName'',
		@PropertyValue = ''''

		EXEC IdentityServerPolicy.AddSyncProperties
		@PropertyName = ''PrimaryComputerPort'',
		@PropertyValue = 80

		EXEC IdentityServerPolicy.AddSyncProperties
		@PropertyName = ''PollDurationInSeconds'',
		@PropertyValue = 300

		EXEC IdentityServerPolicy.AddSyncProperties
		@PropertyName = ''LastSyncFromPrimaryComputerName'',
		@PropertyValue = ''''

		DECLARE @lastSyncTime datetime
		SELECT @lastSyncTime = getutcdate()
		EXEC IdentityServerPolicy.AddSyncProperties
		@PropertyName = ''LastSyncTime'',
		@PropertyValue = @lastSyncTime

		EXEC IdentityServerPolicy.AddSyncProperties
		@PropertyName = ''LastSyncStatus'',
		@PropertyValue = 0

		EXEC IdentityServerPolicy.CreateAuthority
		@ObjectId = @id OUTPUT,
		@Name = ''SelfAuthority'',
		@EntityId = ''SELF AUTHORITY'',
		@IdentityType = ''http://schemas.microsoft.com/ws/2009/12/identityserver/principaltypes/uri'',
		@SignatureAlgorithm = ''http://www.w3.org/2001/04/xmldsig-more#rsa-sha256''

		EXEC IdentityServerPolicy.AddAuthorityPolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyEngine, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''c:[] => issue(claim = c);'',
		@PolicyUsage = 0

		EXEC IdentityServerPolicy.AddAuthorityIdentity
		@ObjectId = @id,
		@IdentityData = ''http://schemas.microsoft.com/ws/2009/12/identityserver/selfauthority'',
		@IdentityKey = ''http://schemas.microsoft.com/ws/2009/12/identityserver/selfauthority''

		EXEC IdentityServerPolicy.CreateAuthority
		@ObjectId = @id OUTPUT,
		@Name = ''Active Directory'',
		@EntityId = ''AD AUTHORITY'',
		@IdentityType = ''http://schemas.microsoft.com/ws/2009/12/identityserver/principaltypes/uri'',
		@SignatureAlgorithm = ''http://www.w3.org/2001/04/xmldsig-more#rsa-sha256''

		EXEC IdentityServerPolicy.AddAuthorityPolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyEngine, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''@RuleTemplate = "PassThroughWindowsAccountName" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughName" @RuleName = "" c:[Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughPrimarySid" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughGroupSid" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughPrimaryGroupSid" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarygroupsid", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughDenyOnlySid" @RuleName = "" c:[Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/denyonlysid", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughDenyOnlyPrimarySid" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/denyonlyprimarysid", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughDenyOnlyPrimaryGroupSid" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/denyonlyprimarygroupsid", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughAuthenticationMethod" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/authenticationmethod", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);
						@RuleTemplate = "PassThroughAuthenticationInstant" @RuleName = "" c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/authenticationinstant", issuer=~"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)$"] => issue(claim = c);'',
		@PolicyUsage = 0

		EXEC IdentityServerPolicy.AddAuthorityIdentity
		@ObjectId = @id,
		@IdentityData = ''http://schemas.microsoft.com/ws/2009/12/identityserver/ADauthority'',
		@IdentityKey = ''http://schemas.microsoft.com/ws/2009/12/identityserver/ADauthority''


		EXEC IdentityServerPolicy.CreateScope
		@ObjectId = @id OUTPUT,
		@Name = ''SelfScope'',
		@IdentityType = ''http://schemas.microsoft.com/ws/2009/12/identityserver/principaltypes/uri'',
		@SignatureAlgorithm = ''http://www.w3.org/2001/04/xmldsig-more#rsa-sha256''

		EXEC IdentityServerPolicy.AddScopePolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyEngine, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''c:[] => issue(claim = c);'' ,
		@PolicyUsage = 0

		EXEC IdentityServerPolicy.AddScopePolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyAuthorizationManager, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''=>issue(type="http://schemas.microsoft.com/authorization/claims/permit", value="true");'',
		@PolicyUsage = 1

		EXEC IdentityServerPolicy.AddScopePolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyAuthorizationManager, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''=>issue(type="http://schemas.microsoft.com/authorization/claims/permit", value="true");'',
		@PolicyUsage = 2

		EXEC IdentityServerPolicy.AddScopePolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyAuthorizationManager, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''=>issue(type="http://schemas.microsoft.com/authorization/claims/permit", value="true");'',
		@PolicyUsage = 3

		EXEC IdentityServerPolicy.AddScopeIdentity
		@ObjectId = @id,
		@IdentityData = ''http://schemas.microsoft.com/ws/2009/12/identityserver/selfscope'',
		@IdentityKey = ''http://schemas.microsoft.com/ws/2009/12/identityserver/selfscope''


		EXEC IdentityServerPolicy.CreateScope
		@ObjectId = @id OUTPUT,
		@Name = ''ProxyTrustProvisionRelyingParty'',
		@IdentityType = ''http://schemas.microsoft.com/ws/2009/12/identityserver/principaltypes/uri'',
		@SignatureAlgorithm = ''http://www.w3.org/2001/04/xmldsig-more#rsa-sha256''


		EXEC IdentityServerPolicy.AddScopePolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyEngine, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''=>issue( store="_ProxyCredentialStore", types=("http://schemas.microsoft.com/ws/2008/06/identity/claims/proxytrustid"),query="ProxyTrustId()");'',
		@PolicyUsage = 0

		EXEC IdentityServerPolicy.AddScopePolicy
		@ObjectId = @id,
		@PolicyType = ''Microsoft.IdentityServer.ClaimsPolicy.Engine.ClaimsPolicyAuthorizationManager, Microsoft.IdentityServer.ClaimsPolicy, Version=6.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=MSIL'',
		@PolicyData = ''exists([Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid", Value == "S-1-5-32-544", Issuer =~ "^AD AUTHORITY$"]) => issue(Type = "http://schemas.microsoft.com/authorization/claims/permit", Value = "true"); 
			c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid", Issuer =~ "^AD AUTHORITY$" ]
					   => issue(store="_ProxyCredentialStore",types=("http://schemas.microsoft.com/authorization/claims/permit"),query="isProxyTrustManagerSid({0})", param=c.Value );
			c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/proxytrustid", Issuer =~ "^SELF AUTHORITY$" ]
					   => issue(store="_ProxyCredentialStore",types=("http://schemas.microsoft.com/authorization/claims/permit"),query="isProxyTrustProvisioned({0})", param=c.Value );'',
		@PolicyUsage = 1

		EXEC IdentityServerPolicy.AddScopeIdentity
		@ObjectId = @id,
		@IdentityData = ''http://schemas.microsoft.com/ws/2009/12/identityserver/proxytrustprovision'',
		@IdentityKey = ''http://schemas.microsoft.com/ws/2009/12/identityserver/proxytrustprovision''


		EXEC IdentityServerPolicy.CreateServiceSettings
		@ObjectId = @id OUTPUT,
		@ServiceSettingsData = N''<?xml version="1.0" encoding="utf-8"?>
		<ServiceSettingsData>
		  <SecurityTokenService>
			<Host Name="localhost" HttpPort="80" HttpsPort="443" NetTcpPort="1501" />
			<Issuer Address="localhost"  FriendlyName="localhost" />
			<Issuance EventLogLevel="15" EnableEventLogThrottling="true" EventLogThrottlingIntervalInMinutes="5"/>
			<!-- Callers with this SID are allowed to send OnBehalfOf requests. -->
			<AllowedOnBehalfOfCallers Sid="" />
			<MexEndpoint Address="/adfs/services/trust/mex" Enabled="true" Mode="Anonymous" Proxy="false" Version="default" CanProxy="false" />
			<PassiveEndpoint Address="/adfs/ls" Enabled="true" Mode="Anonymous" Proxy="false" Version="default" CanProxy="false" />
			<PreventTokenReplay Value="false" ReplayCacheExpirationInterval="60"/>
			<PpidPrivacyEntropy></PpidPrivacyEntropy>
			<ExtendedProtectionTokenCheck>Allow</ExtendedProtectionTokenCheck>
			<TrustEndpoints>
			  <Endpoint Address="/adfs/services/trust/2005/windows" Mode="Windows" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="false" />
			  <Endpoint Address="/adfs/services/trust/2005/windowsmixed" Mode="WindowsMixed" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="false"/>
			  <Endpoint Address="/adfs/services/trust/2005/windowstransport" Mode="WindowsTransport" Proxy="true" Version="wstrust2005" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/certificate" Mode="Certificate" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/certificatemixed" Mode="CertificateMixed" Proxy="true" Version="wstrust2005" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/certificatetransport" Mode="CertificateTransport" Proxy="true" Version="wstrust2005" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/username" Mode="Username" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/usernamebasictransport" Mode="UsernameBasicTransport" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/usernamemixed" Mode="UsernameMixed" Proxy="true" Version="wstrust2005" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/kerberosmixed" Mode="KerberosMixed" Proxy="false" Version="wstrust2005" Enabled="true" CanProxy="false"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenasymmetricbasic256"  Mode="IssuedTokenAsymmetricBasic256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenasymmetricbasic256sha256"  Mode="IssuedTokenAsymmetricBasic256Sha256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenmixedasymmetricbasic256" Mode="IssuedTokenMixedAsymmetricBasic256" Proxy="true" Version="wstrust2005" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenmixedasymmetricbasic256sha256" Mode="IssuedTokenMixedAsymmetricBasic256Sha256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenmixedsymmetricbasic256" Mode="IssuedTokenMixedSymmetricBasic256" Proxy="true" Version="wstrust2005" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenmixedsymmetricbasic256sha256" Mode="IssuedTokenMixedSymmetricBasic256Sha256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokensymmetricbasic256" Mode="IssuedTokenSymmetricBasic256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokensymmetricbasic256sha256" Mode="IssuedTokenSymmetricBasic256Sha256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokensymmetrictripledes" Mode="IssuedTokenSymmetricTripleDes" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokensymmetrictripledessha256" Mode="IssuedTokenSymmetricTripleDesSha256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenmixedsymmetrictripledes" Mode="IssuedTokenMixedSymmetricTripleDes" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/2005/issuedtokenmixedsymmetrictripledessha256" Mode="IssuedTokenMixedSymmetricTripleDesSha256" Proxy="false" Version="wstrust2005" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/kerberosmixed" Mode="KerberosMixed" Proxy="false" Version="wstrust13" Enabled="true" CanProxy="false"/>
			  <Endpoint Address="/adfs/services/trust/13/certificate" Mode="Certificate" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/certificatemixed" Mode="CertificateMixed" Proxy="true" Version="wstrust13" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/certificatetransport" Mode="CertificateTransport" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/username" Mode="Username" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/usernamebasictransport" Mode="UsernameBasicTransport" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/usernamemixed" Mode="UsernameMixed" Proxy="true" Version="wstrust13" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenasymmetricbasic256"  Mode="IssuedTokenAsymmetricBasic256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenasymmetricbasic256sha256"  Mode="IssuedTokenAsymmetricBasic256Sha256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenmixedasymmetricbasic256" Mode="IssuedTokenMixedAsymmetricBasic256" Proxy="true" Version="wstrust13" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenmixedasymmetricbasic256sha256" Mode="IssuedTokenMixedAsymmetricBasic256Sha256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenmixedsymmetricbasic256" Mode="IssuedTokenMixedSymmetricBasic256" Proxy="true" Version="wstrust13" Enabled="true" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenmixedsymmetricbasic256sha256" Mode="IssuedTokenMixedSymmetricBasic256Sha256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokensymmetricbasic256" Mode="IssuedTokenSymmetricBasic256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokensymmetricbasic256sha256" Mode="IssuedTokenSymmetricBasic256Sha256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokensymmetrictripledes" Mode="IssuedTokenSymmetricTripleDes" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokensymmetrictripledessha256" Mode="IssuedTokenSymmetricTripleDesSha256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenmixedsymmetrictripledes" Mode="IssuedTokenMixedSymmetricTripleDes" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/> 
			  <Endpoint Address="/adfs/services/trust/13/issuedtokenmixedsymmetrictripledessha256" Mode="IssuedTokenMixedSymmetricTripleDesSha256" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="true"/>           
			  <Endpoint Address="/adfs/services/trust/13/windows" Mode="Windows" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="false"/>
			  <Endpoint Address="/adfs/services/trust/13/windowsmixed" Mode="WindowsMixed" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="false"/>
			  <Endpoint Address="/adfs/services/trust/13/windowstransport" Mode="WindowsTransport" Proxy="false" Version="wstrust13" Enabled="false" CanProxy="false"/>
			  <Endpoint Address="/adfs/services/trusttcp/windows" Mode="LocalWindows" Proxy="false" Version="wstrust2005" Enabled="true" CanProxy="false"/>
			</TrustEndpoints>
			<ADFSv1MetadataEndpoint Address="/adfs/fs/federationserverservice.asmx" Enabled="true" Mode="Anonymous" Proxy="false" Version="default" CanProxy="false"/>
			<MetadataEndpoint Address="/metadata/" Enabled="true" Mode="Anonymous" Proxy="true" Version="default" CanProxy="true"/>
			<AttributeStores>
			  <Store Id="956FF77F-753E-4C14-836B-40E1FEE532ED" Name="Active Directory" Type="Microsoft.IdentityServer.ClaimsPolicy.Engine.AttributeStore.Ldap.EnterpriseLdapAttributeStore, Microsoft.IdentityServer.ClaimsPolicy">
			  </Store>
			</AttributeStores>
			<LdapAttributeStoreConfiguration 
				 ConnectionCacheRefreshIntervalInMinutes="30" ConnectionCacheMaxSize="50"
				 EventLogThrottlingIntervalInMinutes="5" SchemaRefreshIntervalInMinutes="5" />
			<SamlProtocol AuthnRequestsSigned="false" WantAssertionsSigned="true" WantAuthnRequestsSigned="false" MessageDeliveryWindowInMinutes="5" >
			  <SupportedAuthenticationContexts>
				<AuthenticationContext>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</AuthenticationContext>
				<AuthenticationContext>urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</AuthenticationContext>
				<AuthenticationContext>urn:oasis:names:tc:SAML:2.0:ac:classes:TLSClient</AuthenticationContext>
				<AuthenticationContext>urn:oasis:names:tc:SAML:2.0:ac:classes:X509</AuthenticationContext>
				<AuthenticationContext>urn:federation:authentication:windows</AuthenticationContext>
				<AuthenticationContext>urn:oasis:names:tc:SAML:2.0:ac:classes:Kerberos</AuthenticationContext>
			  </SupportedAuthenticationContexts>
			</SamlProtocol>
			<SsoConfiguration TokenLifetime="480" />
			<ProxyTrustConfiguration TokenLifetime="21600" ProxyTrustId="fdca3f1f-25eb-4a43-99f7-46309d876a25" />
		  </SecurityTokenService>
		  <PolicyStore>
			<AuthorizationPolicy>
			  exists([Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid", Value == "S-1-5-32-544"]) 
				=&gt; issue(Type = "http://schemas.microsoft.com/authorization/claims/permit", Value = "true");
			</AuthorizationPolicy>
			<AuthorizationPolicyReadOnly>
			  exists([Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid", Value == "S-1-5-32-544"]) 
				=&gt; issue(Type = "http://schemas.microsoft.com/authorization/claims/permit", Value = "true");
			</AuthorizationPolicyReadOnly>
			<AuthorizationPolicyReadOnlyNonSensitive>
			  exists([Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid", Value == "S-1-5-32-544"]) 
				=&gt; issue(Type = "http://schemas.microsoft.com/authorization/claims/permit", Value = "true");
			  c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/proxytrustid", Issuer =~ "^SELF AUTHORITY$" ]
			  =&gt; issue(store="_ProxyCredentialStore",types=("http://schemas.microsoft.com/authorization/claims/permit"),query="isProxyTrustProvisioned({0})", param=c.Value );
			</AuthorizationPolicyReadOnlyNonSensitive>
			<Topology>Standalone</Topology>
			<RolloverSettings Enabled="false" />
			<DkmSettings Enabled="false">
			  <Group>00000000-0000-0000-0000-000000000000</Group>
			  <ContainerName>CN=ADFS</ContainerName>
			</DkmSettings>
			<FarmId>00000000-0000-0000-0000-000000000000</FarmId>
		  </PolicyStore>
		  <ArtifactService>
			<SamlEndpoint Address="/adfs/services/trust/artifactresolution" Enabled="true" Mode="Anonymous" Proxy="false" Version="default" CanProxy="true"/>
		  </ArtifactService>
		  <TrustManagementService />
		  <EndpointMetadata>
			<ItemInfo ItemName="MexEndpoint" Protocol="WS-Mex" ServiceName="SecurityTokenService" CanDisable="false" />
			<ItemInfo ItemName="PassiveEndpoint" Protocol="WS-FederationPassive" ServiceName="SecurityTokenService" CanDisable="true" />
			<ItemInfo ItemName="TrustEndpoints" Protocol="WS-Trust" ServiceName="SecurityTokenService" CanDisable="true" />
			<ItemInfo ItemName="MetadataEndpoint" Protocol="Federation Metadata" ServiceName="SecurityTokenService" CanDisable="true" />
			<ItemInfo ItemName="SamlEndpoint" Protocol="SAML-ArtifactResolution" ServiceName="ArtifactService" CanDisable="true" />
			<ItemInfo ItemName="ADFSv1MetadataEndpoint" Protocol="ADFS 1.0 Metadata" ServiceName="SecurityTokenService" CanDisable="true" />
		  </EndpointMetadata>
		</ServiceSettingsData>''

		EXEC IdentityServerPolicy.AddLeasedTask
		@TaskName = ''ArtifactStorageExpiration''

		EXEC IdentityServerPolicy.AddLeasedTask
		@TaskName = ''TrustMonitoring''

		EXEC IdentityServerPolicy.AddLeasedTask
		@TaskName = ''CertificateRollover''

		EXEC IdentityServerPolicy.AddLeasedTask
		@TaskName = ''CertificateExpirationCheck''
	END
	'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IdentityServerPolicy].[ClearAdfsConfiguration]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [IdentityServerPolicy].[ClearAdfsConfiguration]
	AS
	BEGIN
		EXEC IdentityServerPolicy.DeleteAuthorities
		EXEC IdentityServerPolicy.DeleteScopes
		EXEC IdentityServerPolicy.DeleteClaimDescriptors
		EXEC IdentityServerPolicy.DeleteServiceSettings
		EXEC IdentityServerPolicy.RemoveLeasedTask
		EXEC IdentityServerPolicy.RemoveAllServiceStateSummary
		EXEC IdentityServerPolicy.RemoveSyncProperties
		EXEC IdentityServerPolicy.RemoveServiceObjectTypeRelationships
		EXEC dbo.IdentityServerNotificationCleanup
	END
	'
END
GO

ALTER DATABASE AdfsConfiguration SET MULTI_USER WITH ROLLBACK IMMEDIATE
GO
